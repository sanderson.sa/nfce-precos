package br.com.sanderson.nfce.modelo.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.EstabelecimentoDTO;
import br.com.sanderson.nfce.util.jpa.JpaUtil;

public class EstabelecimentoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public List<Estabelecimento> listar() {
		TypedQuery<Estabelecimento> query = manager.createQuery("from Estabelecimento", Estabelecimento.class);
		return query.getResultList();
	}
	
	/**
	 * Métodos chamados da classe ServiceController não são injetados
	 * @return
	 */
	public List<Estabelecimento> listarRest() {
		TypedQuery<Estabelecimento> query = JpaUtil.getEntityManager().createQuery("from Estabelecimento", Estabelecimento.class);
		return query.getResultList();
	}
	
	public Estabelecimento getById(Long id) {
		return manager.find(Estabelecimento.class, id);
	}
	
	public Estabelecimento findByNomeFantasia(String nomeFantasia) {
		Query query = manager.createNamedQuery("Estabelecimento.findByNomeFantasia");
		query.setParameter("nomeFantasia", nomeFantasia);
		try {
			return (Estabelecimento) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}		
	}
	
	public Estabelecimento findByRazaoSocial(String razaoSocial) {
		Query query = manager.createNamedQuery("Estabelecimento.findByRazaoSocial");
		query.setParameter("razaoSocial", razaoSocial);
		try {
			return (Estabelecimento) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<EstabelecimentoDTO> getEstabelecimentosTotal(Long id) {		
		List<EstabelecimentoDTO> lista = new ArrayList<EstabelecimentoDTO>();
		String sql = "SELECT DISTINCT(nf.id_estabelecimento),est.nome_fantasia, count(id_estabelecimento)as notas, sum(nf.vl_nfce) as vl_total FROM public.nota_fiscal nf JOIN public.estabelecimento est ON est.\"id\" = nf.id_estabelecimento WHERE nf.id_usuario = :id GROUP BY nf.id_estabelecimento, est.nome_fantasia";
		Query q = manager.createNativeQuery(sql);
		q.setParameter("id", id);
		
		List<Object[]> ob = q.getResultList();
		for (Object[] o : ob){ 
			EstabelecimentoDTO item = new EstabelecimentoDTO(o[0], o[1],o[2], o[3]);			
			lista.add(item);
		}
		Collections.sort(lista, (item1, item2) -> item2.getVlTotalEstabelecimento().compareTo(item1.getVlTotalEstabelecimento()));
		return lista;
	}
	
	public Estabelecimento getByCnpj(Long cnpj) {
		Query query = manager.createNamedQuery("Estabelecimento.findByCnpj");
		query.setParameter("cnpj", cnpj);
		try {
			return (Estabelecimento) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}		
	}
	
	public void adicionar(Estabelecimento estab) throws PersistenceException {		
		manager.persist(estab);		
	}
	
	public void editar(Estabelecimento estab) {
		manager.merge(estab);
	}
	

}
