package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.ListaCompra;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.modelo.repository.EstabelecimentoRepository;
import br.com.sanderson.nfce.modelo.repository.ProdutoRepository;
import br.com.sanderson.nfce.service.ListaCompraService;


@Model
@ViewScoped
public class ListaComprasBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Usuario usuario = new Usuario();
	private List<Produto> listaCompraProdutos = new ArrayList<Produto>();
	private List<Estabelecimento> listaCompraEstabelecimento = new ArrayList<Estabelecimento>();
	
	private String StrProduto;
	private String StrQuantidade;
	private String StrEstabelecimento;
	private String StrListaDescricao;
	private ListaCompra listaDeCompra;

	@Inject
	ProdutoRepository produtoRepository;
	
	@Inject
	private ListaCompraService listaCompraService;
	
	@Inject
	EstabelecimentoRepository estabRepository;


	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Produto> getListaCompraProdutos() {
		return listaCompraProdutos;
	}

	public void setListaCompraProdutos(List<Produto> listaCompraProdutos) {
		this.listaCompraProdutos = listaCompraProdutos;
	}
	
	public void removerListaDeCompras(){
		//remove lista
	}
	
	public void visualizarListaDeCompras(){
		//remove lista
		System.out.println(" ## Lista de Compras: " + this.listaDeCompra.getTxListaDescricao());
		for(Produto p : this.listaDeCompra.getProduto()){
			System.out.println("Prod: " + p.getDescricao());
		}
	}
	
	public void adicionarProdutoLista(){
		//https://stackoverflow.com/questions/14928912/using-bootstrap-related-tags-inside-jsf2-hinputtext-component
		//https://github.com/bassjobsen/Bootstrap-3-Typeahead
		//$("#cform")[0].reset();
		//document.getElementById("cform").reset();
		
//		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//		String prodDescricao = req.getParameter("produtos");
		
		//Produto p = produtoRepository.findByDescricao(StrProduto);
		String produtoCodigo = this.StrProduto.trim().split("-")[0];
		Produto p = produtoRepository.getByCodigoProduto(new BigInteger(produtoCodigo)).get(0);
		
//		if(StrQuantidade.contains(",")) 
//			p.setQtd(new Float(StrQuantidade.replaceAll(",", ".")));
//		else p.setQtd(new Float(StrQuantidade));
		
		//Produto p = produtoRepository.findByDescricao(StrProduto);
		listaCompraProdutos.add(p);
		StrProduto = "";
		StrQuantidade = "";
	}
	
	public void adicionarEstabelecimentoLista() {
		Estabelecimento e = estabRepository.findByRazaoSocial(StrEstabelecimento.split("-")[0].trim());
		listaCompraEstabelecimento.add(e);
		StrEstabelecimento = "";
	}	
	
	public void compararListaCompras() {
		System.out.println(" ## Gerando lista de preços ...");
		for (Estabelecimento e : listaCompraEstabelecimento) {
			e.getProdutoList().clear();
			for(Produto p : listaCompraProdutos){
				List<Produto> lProdutos = produtoRepository.getByEstabelecimentoAndProduto(e.getId(), p.getCodigoProduto());				
				if(lProdutos != null && !lProdutos.isEmpty()){
					p = lProdutos.get(0);
					p.setExisteNaListaCompra(true);
				}
				else {
					p.setExisteNaListaCompra(false);
				}
				e.getProdutoList().add(p);
			}			
		}
		//Salvar Lista de Compra ao Finalizar
		//salvarListaDeCompra();
	}
	
	private void salvarListaDeCompra() {
		// Salvar Lista
		try {
			ListaCompra listaCompra = new ListaCompra();
			listaCompra.setDtCriacao(new Date());
			listaCompra.setIdUsuario(usuario);
			listaCompra.setTxListaDescricao(this.StrListaDescricao);
			// listaCompra.setProduto(listaCompraProdutos);
			listaCompraService.salvar(listaCompra);
			atualizarProdutos(listaCompra);
			System.out.println("Lista de Compras Salva com sucesso! ");
		} catch (Exception e) {
			System.out.println("Erro ao Salvar Lista de Compras: " + e.getMessage());
		}
	}
	
	private void atualizarProdutos(ListaCompra lcompra){
		try {
			for(Produto p : listaCompraProdutos){
				p.setListacompra(lcompra);
				produtoRepository.guardar(p);
				lcompra.adiciona(p);
			}
			listaCompraService.atualizar(lcompra);
		} catch (Exception e) {
			System.out.println("Erro ao atualizar produtos: " + e.getMessage());
		}
	}
	
	public Produto getProdutoPorEstabelecimento(Produto p, Estabelecimento e){
		for(Produto produto : e.getProdutoList()){
			if(p.getCodigoProduto().equals(produto.getCodigoProduto())) return produto;
		}
		//p.setVlProduto(BigDecimal.ZERO);
		return p; // Workround!!
	}
	
	//remover produto da lista de compras
	public void removeProdutoLista(Produto p){
		listaCompraProdutos.remove(p);
	}
	
	//remover estabelecimento da lista de compras
	public void removeEstabelecimentoLista(Estabelecimento e){
		listaCompraEstabelecimento.remove(e);
	}
	
	//Pesquisar produto Preço
	
	public String getStrProduto() {
		return StrProduto;
	}

	public void setStrProduto(String strProduto) {
		StrProduto = strProduto;
	}

	public String getStrQuantidade() {
		return StrQuantidade;
	}

	public void setStrQuantidade(String strQuantidade) {
		StrQuantidade = strQuantidade;
	}

	public List<Estabelecimento> getListaCompraEstabelecimento() {
		return listaCompraEstabelecimento;
	}

	public void setListaCompraEstabelecimento(
			List<Estabelecimento> listaCompraEstabelecimento) {
		this.listaCompraEstabelecimento = listaCompraEstabelecimento;
	}
	
	public String getStrEstabelecimento() {
		return StrEstabelecimento;
	}

	public void setStrEstabelecimento(String strEstabelecimento) {
		StrEstabelecimento = strEstabelecimento;
	}

	public String getStrListaDescricao() {
		return StrListaDescricao;
	}

	public void setStrListaDescricao(String strListaDescricao) {
		StrListaDescricao = strListaDescricao;
	}

	public ListaCompra getListaDeCompra() {
		return listaDeCompra;
	}

	public void setListaDeCompra(ListaCompra listaDeCompra) {
		this.listaDeCompra = listaDeCompra;
	}

}
