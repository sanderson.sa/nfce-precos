package br.com.sanderson.nfce.bean;

import java.io.Serializable;

import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.service.UsuarioService;
import br.com.sanderson.nfce.util.utilidades.Uteis;

@Model
@ViewScoped
public class LoginBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private String login;
	private String senha;
	private Usuario usuario;

	@Inject
	private UsuarioService service;

	public Usuario GetUsuarioSession() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return (Usuario)facesContext.getExternalContext().getSessionMap().get("user");
	}

	public String logout(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "inicio.xhtml?faces-redirect=true";
	}

	//https://stackoverflow.com/questions/13047446/how-can-i-access-session-attribute-in-facelets-page
	public String login() {
		if(StringUtils.isEmpty(login) || StringUtils.isBlank(login)){
			Uteis.MensagemAlerta("Favor informar o login!");
			return null;
		}
		else if(StringUtils.isEmpty(senha) || StringUtils.isBlank(senha)){
			Uteis.MensagemAlerta("Favor informara senha!");
			return null;
		}
		else{
			usuario = service.login(login, senha);
			if(usuario!= null) {

				FacesContext facesContext = FacesContext.getCurrentInstance();
				facesContext.getExternalContext().getSessionMap().put("user", usuario);
				return "inicio?faces-redirect=true";
			}
			else{
				Uteis.MensagemAlerta("Não foi possível efetuar o login com esse usuário e senha!");
				return null;
			}
		}
	}



	//Getters
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}


}
