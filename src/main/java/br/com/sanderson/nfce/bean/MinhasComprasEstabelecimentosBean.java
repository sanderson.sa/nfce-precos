package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.EstabelecimentoDTO;
import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.service.EstabelecimentoService;

@Model
@ViewScoped
public class MinhasComprasEstabelecimentosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<EstabelecimentoDTO> estabelecimentos = new ArrayList<EstabelecimentoDTO>();
		
	private Usuario usuario = new Usuario();
	
	@Inject
	EstabelecimentoService service;

	public void inicializar() {
		estabelecimentos = service.estabelecimentos(usuario.getId());
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getNumeroNotas(){
		Integer tNotas = estabelecimentos.stream().mapToInt(EstabelecimentoDTO::getNuNotas).sum();
//		double sum2 = NUMBERS_FOR_SUM
//	            .stream()
//	            .mapToDouble(Double::doubleValue)
//	            .sum();
		return tNotas + "";
	}
	
	public String getTotalCompras(){
		BigDecimal sum = estabelecimentos.isEmpty() ? new BigDecimal(0) : estabelecimentos.stream().map(EstabelecimentoDTO::getVlTotalEstabelecimento).reduce(BigDecimal::add).get();
		return "R$" + sum.toString();
	}

	public List<EstabelecimentoDTO> getEstabelecimentos() {
		return estabelecimentos;
	}

	public void setEstabelecimentos(List<EstabelecimentoDTO> estabelecimentos) {
		this.estabelecimentos = estabelecimentos;
	}
	
}
