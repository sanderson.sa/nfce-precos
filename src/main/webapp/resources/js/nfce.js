type = ["", "info", "success", "warning", "danger"], $().ready(function() {
  $sidebar = $(".sidebar"), $off_canvas_sidebar = $(".off-canvas-sidebar"), window_width = $(window).width(), window_width > 767 && $(".fixed-plugin .dropdown").hasClass("show-dropdown") && $(".fixed-plugin .dropdown").addClass("open"), jQuery.extend(jQuery.validator.messages, {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Por favor informe uma e-mail válido: email@dominio.com.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Por favor informe pelo menos {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
  }), $(".fixed-plugin a").click(function(e) {
    $(this).hasClass("switch-trigger") && (e.stopPropagation ? e.stopPropagation() : window.event && (window.event.cancelBubble = !0))
  }), $(".fixed-plugin .background-color span").click(function() {
    $(this).siblings().removeClass("active"), $(this).addClass("active");
    var e = $(this).data("color");
    0 != $sidebar.length && $sidebar.attr("data-background-color", e), 0 != $off_canvas_sidebar.length && $off_canvas_sidebar.attr("data-background-color", e)
  }), $(".fixed-plugin .active-color span").click(function() {
    $(this).siblings().removeClass("active"), $(this).addClass("active");
    var e = $(this).data("color");
    0 != $sidebar.length && $sidebar.attr("data-active-color", e), 0 != $off_canvas_sidebar.length && $off_canvas_sidebar.attr("data-active-color", e)
  }), 0 != $("#twitter").length && $("#twitter").sharrre({
    share: {
      twitter: !0
    },
    enableHover: !1,
    enableTracking: !0,
    buttons: {
      twitter: {
        via: "CreativeTim"
      }
    },
    click: function(e, t) {
      e.simulateClick(), e.openPopup("twitter")
    },
    template: '<i class="fa fa-twitter"></i>',
    url: "http://demos.creative-tim.com/paper-dashboard-pro/examples/dashboard/overview.html"
  }), 0 != $("#facebook").length && $("#facebook").sharrre({
    share: {
      facebook: !0
    },
    enableHover: !1,
    enableTracking: !0,
    click: function(e, t) {
      e.simulateClick(), e.openPopup("facebook")
    },
    template: '<i class="fa fa-facebook-square"></i>',
    url: "http://demos.creative-tim.com/paper-dashboard-pro/examples/dashboard/overview.html"
  })
}), nfce = {
  initGoogleMaps: function() {
    var e = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(-1.458343, -48.480694),
        zoom: 13,
        mapTypeControl: !1
      }),
      t = new google.maps.InfoWindow;
    $.get("/rest/service/estabelecimentosCoordenadas", function(a) {
      var n = a.map(function(e) {
          return {
            value: e.titulo,
            data: {
              id: e.id,
              type: "titulo",
              coordenadas: e.coordenadas
            }
          }
        }),
        i = a.filter(function(e) {
          return 0 !== e.coordenadas.lat
        }),
        r = (i.map(function(e) {
          return {
            value: e.endereco.logradouro,
            data: {
              id: e.id,
              type: "rua",
              coordenadas: e.coordenadas
            }
          }
        }), [].concat(n));
      $("#pac-input").autocomplete({
        lookup: r,
        noCache: !0,
        onSelect: function(t) {
          "rua" === t.data.type ? (e.setZoom(18), e.setCenter(t.data.coordenadas)) : window.location.href = "/estabelecimento-detalhe?id=" + t.data.id
        }
      });
      var o = document.getElementById("pac-input");
      e.controls[google.maps.ControlPosition.TOP_LEFT].push(o), $.each(a, function(a, n) {
        var i = document.createElement("div"),
          r = document.createElement("strong");
        r.textContent = n.titulo, i.appendChild(r), i.appendChild(document.createElement("br"));
        var o = document.createElement("text");
        o.textContent = n.descricao, i.appendChild(o);
        var s = new google.maps.LatLng(parseFloat(n.lat), parseFloat(n.lng)),
          l = new google.maps.Marker({
            map: e,
            position: s
          });
        l.addListener("click", function() {
          t.setContent(i), t.open(e, l)
        })
      })
    })
  },
  checkFullPageBackgroundImage: function() {
    $page = $(".full-page"), image_src = $page.data("image"), void 0 !== image_src && (image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>', $page.append(image_container))
  },
  initWizard: function() {
    $(document).ready(function() {
      var e = $("#wizardForm").validate({
        rules: {
          email: {
            required: !0,
            email: !0,
            minlength: 5
          },
          first_name: {
            required: !1,
            minlength: 5
          },
          last_name: {
            required: !1,
            minlength: 5
          },
          website: {
            required: !0,
            minlength: 5,
            url: !0
          },
          framework: {
            required: !1,
            minlength: 4
          },
          cities: {
            required: !0
          },
          price: {
            number: !0
          }
        }
      });
      $("#wizardCard").bootstrapWizard({
        tabClass: "nav nav-pills",
        nextSelector: ".btn-next",
        previousSelector: ".btn-back",
        onNext: function(t, a, n) {
          if (!$("#wizardForm").valid()) return e.focusInvalid(), !1
        },
        onInit: function(e, t, a) {
          var n = t.find("li").length;
          $width = 100 / n, $display_width = $(document).width(), $display_width < 600 && n > 3 && ($width = 50), t.find("li").css("width", $width + "%")
        },
        onTabClick: function(e, t, a) {
          return !1
        },
        onTabShow: function(e, t, a) {
          var n = t.find("li").length,
            i = a + 1,
            r = t.closest(".card-wizard");
          i >= n ? ($(r).find(".btn-next").hide(), $(r).find(".btn-finish").show()) : 1 == i ? $(r).find(".btn-back").hide() : ($(r).find(".btn-back").show(), $(r).find(".btn-next").show(), $(r).find(".btn-finish").hide())
        }
      })
    })
  },
};