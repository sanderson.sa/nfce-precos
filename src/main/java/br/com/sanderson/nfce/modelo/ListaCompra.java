/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sanderson.nfce.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sanderson
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListaCompra.findAll", query = "SELECT l FROM ListaCompra l"),
    @NamedQuery(name = "ListaCompra.findById", query = "SELECT l FROM ListaCompra l WHERE l.id = :id"),
    @NamedQuery(name = "ListaCompra.findByUsuario", query = "SELECT l FROM ListaCompra l WHERE l.idUsuario = :idUsuario"),
    @NamedQuery(name = "ListaCompra.findByTxListaDescricao", query = "SELECT l FROM ListaCompra l WHERE l.txListaDescricao = :txListaDescricao"),
    @NamedQuery(name = "ListaCompra.findByDtCriacao", query = "SELECT l FROM ListaCompra l WHERE l.dtCriacao = :dtCriacao")})
public class ListaCompra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "tx_lista_descricao")
    private String txListaDescricao;
    @Column(name = "dt_criacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCriacao;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="listacompra")
    private List<Produto> produto = new ArrayList<Produto>();
    
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne
    private Usuario idUsuario;

    public ListaCompra() {
    }

    public ListaCompra(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTxListaDescricao() {
        return txListaDescricao;
    }

    public void setTxListaDescricao(String txListaDescricao) {
        this.txListaDescricao = txListaDescricao;
    }

    public Date getDtCriacao() {
        return dtCriacao;
    }

    public void setDtCriacao(Date dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

	public List<Produto> getProduto() {
		List<Produto> listaSegura = Collections.unmodifiableList(this.produto);  
        return listaSegura;
	}

//	public void setProduto(List<Produto> produto) {
//		this.produto = produto;
//	}

	public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaCompra)) {
            return false;
        }
        ListaCompra other = (ListaCompra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return txListaDescricao;
    }
    
    public void adiciona(Produto item) {
        this.produto.add(item);
        item.setListacompra(this); // mantém a consistência
    }
    
}
