package br.com.sanderson.nfce.service;

import java.io.Serializable;

import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.modelo.repository.UsuarioRepository;
import br.com.sanderson.nfce.util.jpa.Transactional;

public class UsuarioService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	UsuarioRepository repository;
	
	@Transactional
	public void salvar(Usuario usuario) throws NegocioException {
		//Regra de negócio aqui.
		
		//Salvar
		this.repository.adicionar(usuario);
	}
	
	@Transactional
	public void atualizar(Usuario usuario) throws NegocioException {
		// Regra de negócio aqui.

		// Atualizar
		this.repository.editar(usuario);
	}
	
	public Usuario login(String login, String senha){
		return repository.login(login, senha);
	}
	

}
