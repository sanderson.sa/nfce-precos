package br.com.sanderson.nfce.rest;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class MapCoordenadas {
	private String id;
	private String titulo;
	private String descricao;
	private String lat;
	private String lng;
	private Coordenadas coordenadas = new Coordenadas();
	private Endereco endereco = new Endereco();
	
	
	public MapCoordenadas() {
		
	}
	
	public MapCoordenadas(String id, String titulo, String descricao, String lat,
			String lng) {
		super();
		this.titulo = titulo;
		this.descricao = descricao;
		this.lat = lat;
		this.lng = lng;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Coordenadas getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
}
