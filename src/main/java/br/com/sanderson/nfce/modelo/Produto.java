/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sanderson.nfce.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.sanderson.nfce.util.utilidades.MD5StringHashing;
import br.com.sanderson.nfce.util.utilidades.Uteis;
import br.inf.portalfiscal.nfe.TNfeProc;
import br.inf.portalfiscal.nfe.TNFe.InfNFe.Det;

/**
 *
 * @author sanderson
 */
@Entity
@Table(name = "produto", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p"),
    @NamedQuery(name = "Produto.findById", query = "SELECT p FROM Produto p WHERE p.id = :id"),
    @NamedQuery(name = "Produto.findDistinct", query = "SELECT DISTINCT(p.codigoProduto) FROM Produto p ORDER BY p.codigoProduto"),
    @NamedQuery(name = "Produto.findByCodigoProduto", query = "SELECT p FROM Produto p WHERE p.codigoProduto = :codigoProduto order by p.idNotafiscal.dataEmissao DESC"),
    @NamedQuery(name = "Produto.findByEan", query = "SELECT p FROM Produto p WHERE p.ean = :ean"),
    @NamedQuery(name = "Produto.findByNcm", query = "SELECT p FROM Produto p WHERE p.ncm = :ncm"),
    @NamedQuery(name = "Produto.findByDescricao", query = "SELECT p FROM Produto p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "Produto.findByVlUniVenda", query = "SELECT p FROM Produto p WHERE p.vlUniVenda = :vlUniVenda"),
    @NamedQuery(name = "Produto.findByVlProduto", query = "SELECT p FROM Produto p WHERE p.vlProduto = :vlProduto"),
    @NamedQuery(name = "Produto.findByUnidade", query = "SELECT p FROM Produto p WHERE p.unidade = :unidade"),
    @NamedQuery(name = "Produto.findByVlUniTributacao", query = "SELECT p FROM Produto p WHERE p.vlUniTributacao = :vlUniTributacao")})
public class Produto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "codigo_produto")
    private BigInteger codigoProduto;
    @Column(name = "ean")
    private BigInteger ean;
    @Column(name = "ncm")
    private BigInteger ncm;
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_uni_venda")
    private BigDecimal vlUniVenda;
    @Column(name = "vl_produto")
    private BigDecimal vlProduto;
    @Column(name = "unidade")
    private String unidade;
    @Column(name = "vl_uni_tributacao")
    private String vlUniTributacao;
    @Column(name = "dt_venda")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtVenda;
   // @Lob
    @Column(name = "img_produto")
    private byte[] imgProduto;
    @JoinColumn(name = "id_estabelecimento", referencedColumnName = "id")
    @ManyToOne
    private Estabelecimento idEstabelecimento;
    @Column(name = "qtd")
    private Float qtd;
    @Column(name = "hash_produto")
    private String hashProduto;
    @JoinColumn(name = "id_notafiscal", referencedColumnName = "id")
    @ManyToOne
    private NotaFiscal idNotafiscal;
    
    @ManyToOne
    private ListaCompra listacompra;
    
    
    
    @Transient
    private boolean existeNaListaCompra;
       

    public ListaCompra getListacompra() {
		return listacompra;
	}

	public void setListacompra(ListaCompra listacompra) {
		this.listacompra = listacompra;
	}

	public boolean isExisteNaListaCompra() {
		return existeNaListaCompra;
	}

	public void setExisteNaListaCompra(boolean existeNaListaCompra) {
		this.existeNaListaCompra = existeNaListaCompra;
	}

	

	public Produto() {
    }
    
    public Produto(String descricao, String quantidade) {
    	this.descricao = descricao;
    	this.qtd = new Float(quantidade.replaceAll(",", "."));
    }

    public Produto(Long id) {
        this.id = id;
    }
    
    public Produto(Det det, Estabelecimento e, TNfeProc nfe){
    	this.codigoProduto = new BigInteger(Uteis.removeCaracteres(det.getProd().getCProd()));
    	this.descricao = det.getProd().getXProd();
    	// str = str.replaceAll("[\\D.]", "");
    	this.ean = det.getProd().getCEAN().replaceAll("[\\D.]", "").isEmpty() ? new BigInteger("999999999") : new BigInteger(det.getProd().getCEAN());
    	this.ncm = det.getProd().getCEAN().replaceAll("[\\D.]", "").isEmpty() ? new BigInteger("999999999") : new BigInteger(det.getProd().getNCM());
    	this.unidade = det.getProd().getUCom();
    	this.vlProduto = new BigDecimal(det.getProd().getVProd());
    	this.vlUniTributacao = det.getProd().getVUnTrib();
    	this.vlUniVenda = new BigDecimal(det.getProd().getVUnCom());
    	this.idEstabelecimento = e;
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	try {
			this.dtVenda = formatter.parse(nfe.getNFe().getInfNFe().getIde().getDhEmi().substring(0, 20));
		} catch (ParseException ex) {
			System.out.println(" ### Erro criar produto, data da emissão: " + ex.getMessage());
		}
    	this.qtd = new Float(det.getProd().getQTrib());    	
    }
    
    public Produto(br.inf.portalfiscal.nfe.v_400.TNFe.InfNFe.Det det, Estabelecimento e, br.inf.portalfiscal.nfe.v_400.TNfeProc nfe){
    	this.codigoProduto = new BigInteger(Uteis.removeCaracteres(det.getProd().getCProd()));
    	this.descricao = det.getProd().getXProd();
    	this.ean = det.getProd().getCEAN().replaceAll("[\\D.]", "").isEmpty() ? new BigInteger("999999999") : new BigInteger(det.getProd().getCEAN());
    	this.ncm = det.getProd().getCEAN().replaceAll("[\\D.]", "").isEmpty() ? new BigInteger("999999999") : new BigInteger(det.getProd().getNCM());
    	this.unidade = det.getProd().getUCom();
    	this.vlProduto = new BigDecimal(det.getProd().getVProd());
    	this.vlUniTributacao = det.getProd().getVUnTrib();
    	this.vlUniVenda = new BigDecimal(det.getProd().getVUnCom());
    	this.idEstabelecimento = e;
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	try {
			this.dtVenda = formatter.parse(nfe.getNFe().getInfNFe().getIde().getDhEmi().substring(0, 20));
		} catch (ParseException ex) {
			System.out.println(" ### Erro criar produto, data da emissão: " + ex.getMessage());
		}
    	this.qtd = new Float(det.getProd().getQTrib());    	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(BigInteger codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public BigInteger getEan() {
        return ean;
    }

    public void setEan(BigInteger ean) {
        this.ean = ean;
    }

    public BigInteger getNcm() {
        return ncm;
    }

    public void setNcm(BigInteger ncm) {
        this.ncm = ncm;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getVlUniVenda() {
        return vlUniVenda;
    }

    public void setVlUniVenda(BigDecimal vlUniVenda) {
        this.vlUniVenda = vlUniVenda;
    }

    public BigDecimal getVlProduto() {
        return vlProduto;
    }

    public void setVlProduto(BigDecimal vlProduto) {
        this.vlProduto = vlProduto;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getVlUniTributacao() {
        return vlUniTributacao;
    }

    public void setVlUniTributacao(String vlUniTributacao) {
        this.vlUniTributacao = vlUniTributacao;
    }

    public NotaFiscal getIdNotafiscal() {
        return idNotafiscal;
    }

    public void setIdNotafiscal(NotaFiscal idNotafiscal) {
        this.idNotafiscal = idNotafiscal;
    }

    public Date getDtVenda() {
		return dtVenda;
	}

	public void setDtVenda(Date dtVenda) {
		this.dtVenda = dtVenda;
	}

	public byte[] getImgProduto() {
		return imgProduto;
	}

	public void setImgProduto(byte[] imgProduto) {
		this.imgProduto = imgProduto;
	}

	public Estabelecimento getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public void setIdEstabelecimento(Estabelecimento idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}

	public Float getQtd() {
		return qtd;
	}

	public void setQtd(Float qtd) {
		this.qtd = qtd;
	}

	public String getHashProduto() {
		return hashProduto;
	}

	public void setHashProduto(String hashProduto) {
		this.hashProduto = hashProduto;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codigoProduto + " - " + descricao;
    }

	public String getDtCompra() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(idNotafiscal.getDataEmissao());
	}

	

	public String getHash() {
		//System.out.println(idNotafiscal.getDataEmissao().toString().trim() + idNotafiscal.getIdEstabelecimento().getId());
		try {
			return MD5StringHashing.hash(Uteis.dataFormatada("DDMMYYYY", idNotafiscal.getDataEmissao()).trim()+idNotafiscal.getIdEstabelecimento().getId()+vlUniTributacao.toString()+vlUniVenda.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
    
}
