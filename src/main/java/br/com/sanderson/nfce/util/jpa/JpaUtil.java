package br.com.sanderson.nfce.util.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {

	private static EntityManagerFactory factory;
	static PersistenceProperties properties = new PersistenceProperties();

	static {
		//Heroku
//		factory = Persistence.createEntityManagerFactory("bemagora_pu", properties.get());
		
		//Local
		factory = Persistence.createEntityManagerFactory("nfce_pu");
	}
	
	public static EntityManager getEntityManager() {
		return factory.createEntityManager();
	}

}