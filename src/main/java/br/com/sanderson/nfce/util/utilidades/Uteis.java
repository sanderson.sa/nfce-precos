package br.com.sanderson.nfce.util.utilidades;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
//import br.com.venus.repository.entity.Usuario;


public class Uteis {

    /** Mascara para CEP. Formato: <b>'##.###-###'</b> */
    public static final String PATTERN_CEP = "##.###-###";
        /** Mascara para CEP. Formato: <b>'##.##-###'</b> */
    public static final String PATTERN_RG = "##.##-###";
    /** Mascara para CNPJ. Formato: <b>'##.###.###/####-##'</b> */
    public static final String PATTERN_CNPJ = "##.###.###/####-##";
    /** Mascara para CPF. Formato: <b>'###.###.###-##'</b> */
    public static final String PATTERN_CPF = "###.###.###-##";
    public static final String VERSAO_3_10 = "3_10";
    public static final String VERSAO_4_00 = "4_00";
    	
	public static boolean isCPF(String cpf){
        CPFValidator vld = new CPFValidator();
        try {
            vld.assertValid(cpf);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static boolean isCNPJ(String cnpj) {       
        CNPJValidator vld = new CNPJValidator();
        try {
            vld.assertValid(cnpj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
	
	public static void msgInfo(String msg){
		// adding info message
        FacesMessage infoMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, 
            "Informação", msg);
        FacesContext.getCurrentInstance().addMessage("", infoMessage);
	}
	
	public static void msgAlerta(String msg){
		// adding warning message
        FacesMessage warningMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, 
            "Alerta", msg);
        FacesContext.getCurrentInstance().addMessage("", warningMessage);
	}
	
	public static void msgErro(String msg){
		// adding error message
        FacesMessage errorMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
            "Error", msg);
        FacesContext.getCurrentInstance().addMessage("", errorMessage);
	}
	
	public static void msgFatal(String msg){
		// adding fatal message
        FacesMessage fatalMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL, 
            "Erro Fatal", msg);
        FacesContext.getCurrentInstance().addMessage("", fatalMessage);
	}
	
	//MOSTRAR MENSAGEM
	public static void MensagemAlerta(String mensagem){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage("Alerta", mensagem));
	}
 
	//MOSTRAR MENSAGEM
	public static void MensagemAtencao(String mensagem){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Atenção:", mensagem));
	}
 
	//MOSTRAR MENSAGEM
	public static void MensagemInfo(String mensagem){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", mensagem));
	}
	
	/**
     * Remove caracteres não numericos.
     * @param numeroFormatado numero com máscara
     * @return apenas numeros
     */
    public static String removeCaracteres(String numeroFormatado) {
        Pattern patternSemCaracteres = Pattern.compile("([0-9])");
//        Pattern patternSemCaracteres = Pattern.compile("\\s");
        if (numeroFormatado == null) {
            return "";
        }
        Matcher matcherNumero = patternSemCaracteres.matcher(numeroFormatado.trim());
        StringBuilder numeroSemFormatacao = new StringBuilder();
        while (matcherNumero.find()) {
            numeroSemFormatacao.append(matcherNumero.group());
        }
        return numeroSemFormatacao.toString();
    }
    
    /**
     * Metodo para validacao de CPF ou CNPJ.
     * @param cpfOrCnpj
     * @return <b>true</b> (CPF ou CNPJ valido); <br />
     * <b>false</b> (CPF ou CNPJ invalido);
     * @author Augusto Rezende
     */
    public static boolean validaCpfCnpj(String cpfOrCnpj) {
        if (cpfOrCnpj == null) {
            return false;
        }
        String n = cpfOrCnpj.replaceAll("[^0-9]*", "");
        if (n.equals("00000000000") || n.equals("11111111111") || n.equals("22222222222") || n.equals("33333333333") || n.equals("44444444444") || n.equals("55555555555") || n.equals("66666666666") || n.equals("77777777777") || n.equals("88888888888") || n.equals("99999999999") || n.equals("00000000000000")) {
            return false;
        }
        boolean isCnpj = n.length() == 14;
        boolean isCpf = n.length() == 11;
        if (!isCpf && !isCnpj) {
            return false;
        }
        int i, j, digit, coeficient, sum;
        int[] foundDv = {0, 0};
        int dv1 = Integer.parseInt(String.valueOf(n.charAt(n.length() - 2)));
        int dv2 = Integer.parseInt(String.valueOf(n.charAt(n.length() - 1)));
        for (j = 0; j < 2; j++) {
            sum = 0;
            coeficient = 2;
            for (i = n.length() - 3 + j; i >= 0; i--) {
                digit = Integer.parseInt(String.valueOf(n.charAt(i)));
                sum += digit * coeficient;
                coeficient++;
                if (coeficient > 9 && isCnpj) {
                    coeficient = 2;
                }
            }
            foundDv[j] = 11 - sum % 11;
            if (foundDv[j] >= 10) {
                foundDv[j] = 0;
            }
        }
        return dv1 == foundDv[0] && dv2 == foundDv[1];
    }
    
    /**
     * Metodo para retornar o CNPJ no formato desejado.<br/> Mascara de
     * CNPJ: <b>##.###.###/####-##</b>
     * @param valor
     * @return
     */
    public static String formatarCNPJ(String valor) {
        while (valor.length() < 14) {
            valor = "0" + valor;
        }
        return formatar(valor, PATTERN_CNPJ);
    }
   /* 
    public static void exibirUsuario(Usuario u){
    	System.out.println(" ## Preparar para Salvar Usuário ##");
    	System.out.println(" ## NOME: " + u.getNome());
    	System.out.println(" ## LOGIN: " + u.getLogin());
    	System.out.println(" ## E-MAIL: " + u.getEmail());
    	if(u.getFone() != null ) System.out.println(" ## FONE: " + u.getFone());
    	if(u.getCelular() != null ) System.out.println(" ## CELULAR: " + u.getCelular());
    	if(u.getDescMaximo() != null) System.out.println(" ## DESCONTO MÁXIMO: " + u.getDescMaximo());
    	if(u.getDescRecebimento() != null) System.out.println(" ## DESC. MAX. RECEBIMENTO: " + u.getDescRecebimento());
    	if(u.getCelular() != null) System.out.println(" ## OBS.: " + u.getCelular());
    	System.out.println(" ### ### ### ### ### ### ### ### ###");
    }*/
    
    public static void exception(Exception e){
    	System.out.println(" ### Exception ###");
    	System.out.println("   ## MESSAGE: " + e.getMessage());
    	System.out.println("   ## CAUSE: " + e.getCause());
    	System.out.println(" ### ## ## ## ## ###");
    }
    
    /**
     * Metodo para formatacao de Documentos e/ou Processos.
     *
     * @author Augusto Rezende
     * @param valor
     * @param mascara
     * @return saida (String)
     */
    public static String formatar(String valor, String mascara) {
        StringBuilder dado = new StringBuilder("");
        for (int i = 0; i < valor.length(); i++) {
            char c = valor.charAt(i);
            if (Character.isDigit(c)) {
                dado.append(c);
            }
        }
        int indMascara = mascara.length();
        int indCampo = dado.toString().length();
        for (; indCampo > 0 && indMascara > 0;) {
            if (mascara.charAt(--indMascara) == '#') {
                indCampo--;
            }
        }
        StringBuilder saida = new StringBuilder("");
        for (; indMascara < mascara.length(); indMascara++) {
            saida.append((mascara.charAt(indMascara) == '#') ? dado.toString().charAt(indCampo++) : mascara.charAt(indMascara));
        }
        return saida.toString();
    }
    
    public static byte [] ImageToByte(InputStream fis) throws FileNotFoundException{
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
            }
        } catch (IOException ex) {
        }
        byte[] bytes = bos.toByteArray();
     
     return bytes; 
    }
    
	private String getStringFromPart(Part arquivo) {
		String conteudo = null;
		try {
			conteudo = new Scanner(arquivo.getInputStream())
					.useDelimiter("\\A").next();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conteudo;
	}
	
	/**
	 * Retorna String formatada a partir de um Date
    	yyyy-MM-dd / yyyy-MM-dd'T'HH:mm:ss
	 * @param formato
	 * @param dt
	 * @return
	 */
	public static String dataFormatada(String formato, Date dt){
		SimpleDateFormat formatter = new SimpleDateFormat(formato);
		return formatter.format(dt);
	}
	
	public static Date dataFromStr(String strDt, String formato) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(formato);
		return formatter.parse(strDt);
	}
    
    
}
