package br.com.sanderson.nfce.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.ListaCompra;
import br.com.sanderson.nfce.modelo.repository.ListaComprasRepository;

@FacesConverter(forClass = ListaCompra.class)
public class ListaCompraConverter implements Converter {
	
	@Inject
	private ListaComprasRepository repository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		ListaCompra retorno = null;
		if (value != null && !"".equals(value)) {
			retorno = this.repository.getById(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			ListaCompra object = ((ListaCompra) value);
			return object.getId() == null ? null : object.getId().toString();
		}
		return null;
	}

}
