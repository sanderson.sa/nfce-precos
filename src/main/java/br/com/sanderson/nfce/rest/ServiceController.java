package br.com.sanderson.nfce.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.json.JSONArray;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.repository.EstabelecimentoRepository;
import br.com.sanderson.nfce.modelo.repository.ProdutoRepository;

@Path("/service")
public class ServiceController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private EstabelecimentoRepository estabelecimentoRepository = new EstabelecimentoRepository();
	private ProdutoRepository produtoRepository = new ProdutoRepository();
	
	/**
	 * Esse método lista todas pessoas cadastradas na base
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/estabelecimentosCoordenadas")
	public List<MapCoordenadas> estabelecimentosCoordenadas(){
		List<Estabelecimento> estabelecimentos =  new ArrayList<Estabelecimento>();
		estabelecimentos = estabelecimentoRepository.listarRest();
		List<MapCoordenadas> listaCoordenadas = new ArrayList<MapCoordenadas>();
		for (Estabelecimento e : estabelecimentos) {			
			if(e.getLatlng() != null && !e.getLatlng().isEmpty()){
				MapCoordenadas m = new MapCoordenadas();
				m.setId(e.getId().toString());				
				m.setTitulo(e.getRazaoSocial());
				m.setDescricao(e.getIdEndereco().enderecoCompleto());
				m.setLat(e.getLatlng().split(",")[0]);
				m.setLng(e.getLatlng().split(",")[1]);
				//m.getCoordenadas().setLat(e.getLatlng().split(",")[0]);
				//m.getCoordenadas().setLng(e.getLatlng().split(",")[1]);
				//m.getEndereco().setLogradouro(e.getIdEndereco().getLogradouro());
				//m.getEndereco().setBairro(e.getIdEndereco().getBairro());
				//m.getEndereco().setCep(e.getIdEndereco().getCep());
				listaCoordenadas.add(m);
			}
		}
		return listaCoordenadas;
	}
	
	
	/**
	 * Esse método lista todos od produtos cadastradas na base
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/produtos")
	public String produtos(){
		String[] listProdutos;
		List<Produto> produtos =  new ArrayList<Produto>();
		produtos = produtoRepository.listarRest();
		listProdutos = new String[produtos.size()];
		for (int i = 0; i < produtos.size(); i++) {
			listProdutos[i] = produtos.get(i).getCodigoProduto() + "-" + produtos.get(i).getDescricao();
		}
		JSONArray mJSONArray = new JSONArray(Arrays.asList(listProdutos));
		return mJSONArray.toString();
	}
	
	/**
	 * Esse método lista todas pessoas cadastradas na base
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/estabcomendereco")
	public String estabComEndereco(){
		String[] lista;
		List<Estabelecimento> estabelecimentos =  new ArrayList<Estabelecimento>();
		estabelecimentos = estabelecimentoRepository.listarRest();
		lista = new String[estabelecimentos.size()];
		for (int i = 0; i < estabelecimentos.size(); i++) {
			lista[i] = estabelecimentos.get(i).getRazaoSocial() + " - " + estabelecimentos.get(i).getIdEndereco().getLogradouro() + " - " + estabelecimentos.get(i).getIdEndereco().getBairro(); 
		}
		JSONArray mJSONArray = new JSONArray(Arrays.asList(lista));
		return mJSONArray.toString();
	}
	
	
	
	/**
	 * Esse método lista todas pessoas cadastradas na base
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/estabelecimentos")
	public List<Estabelecimento> estabelecimentos(){
		List<Estabelecimento> estabelecimentos =  new ArrayList<Estabelecimento>();
		estabelecimentos = estabelecimentoRepository.listarRest();
		return estabelecimentos;
	}
	
	/**
	 * Esse método busca uma pessoa cadastrada pelo código
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/estabelecimentos/{cnpj}")
	public Estabelecimento GetPessoa(@PathParam("cnpj") Long cnpj){
 
//		Estabelecimento e = estabService.getByCNPJ(cnpj);
// 
//		if(e != null)
//			return e;
 
		return null;
	}
	
}
