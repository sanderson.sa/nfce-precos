package br.com.sanderson.nfce.bean;

import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.servlet.http.Part;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.NotaFiscal;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.service.EstabelecimentoService;
import br.com.sanderson.nfce.service.NegocioException;
import br.com.sanderson.nfce.service.NotaFiscalService;
import br.com.sanderson.nfce.service.ProdutoService;
import br.com.sanderson.nfce.util.utilidades.Uteis;
import br.inf.portalfiscal.nfe.TNFe.InfNFe.Det;
import br.inf.portalfiscal.nfe.TNFe;
import br.inf.portalfiscal.nfe.TNfeProc;


@Model
@ViewScoped
public class ImportarXmlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Part arquivo;
	private List<Part> files;
	private Estabelecimento estabelecimento;
	private NotaFiscal notaFiscal;
	private List<Produto> produtos = new ArrayList<Produto>();
	private String VERSAO;	
		
	static byte[] xml;	
	
	
	private Usuario usuario = new Usuario();
	
	@Inject
	private NotaFiscalService notaFiscalservice;
	
	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private EstabelecimentoService estabelecimentoService;
	
	//Temp
	private String numeroNf;
	private String protocoloNf;
	private String dataEmissaoNf;
	private String localNf;
	
	public void importarXML() {
		try {
			if(arquivo != null) {
				InputStream in = arquivo.getInputStream();				
				Scanner scanner = new Scanner(in);
				String conteudo = scanner.useDelimiter("\\A").next();
				scanner.close();
				if(conteudo.contains("versao=\"4.00\"")) {
					VERSAO = Uteis.VERSAO_4_00;
				}else if(conteudo.contains("versao=\"3.10\"")) {
					VERSAO = Uteis.VERSAO_3_10;
				}
				xml = conteudo.getBytes(Charset.forName("UTF-8"));
				if (conteudo != null && !conteudo.isEmpty())
					if(VERSAO.equals(Uteis.VERSAO_3_10)) {						
						visualizarNfe(getNFeV3_10(conteudo));
					} else if(VERSAO.equals(Uteis.VERSAO_4_00)) {
						visualizarNfeV4_00(getNFeV4_00(conteudo));
					}
				}
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}
	
	private List<Produto> getListaProdutosFromXML(List<Det> dets, Estabelecimento e, TNfeProc nfe) throws Exception{
		List<Produto> prods = new ArrayList<Produto>();
		//Validar XML
		prods = preencherListaProdutos(dets, e , nfe);
		return prods;
	}
	
	private List<Produto> getListaProdutosFromXMLV4_00(List<br.inf.portalfiscal.nfe.v_400.TNFe.InfNFe.Det> dets, Estabelecimento e, br.inf.portalfiscal.nfe.v_400.TNfeProc nfe) throws Exception{
		List<Produto> prods = new ArrayList<Produto>();
		//Validar XML
		prods = preencherListaProdutosV4_00(dets, e , nfe);
		return prods;
	}
	
	private String versao(InputStream in) {				
		Scanner scanner = new Scanner(in);
		String conteudo = scanner.useDelimiter("\\A").next();
		scanner.close();
		if(conteudo.contains("versao=\"4.00\"")) {
			return Uteis.VERSAO_4_00;
		} else if(conteudo.contains("versao=\"3.10\"")) {
			return Uteis.VERSAO_3_10;
		}
		return Uteis.VERSAO_3_10;
	}
	
	public void importarListaXML() {
	    try{
			if (files != null) {
		        for (Part file : files) {
		            if(versao(file.getInputStream()).equals(Uteis.VERSAO_3_10)) {
		            	InputStream content = file.getInputStream();
			            TNfeProc nfce = getNFe(content);
			            String cnpjEstabelecimento = nfce.getNFe().getInfNFe().getEmit().getCNPJ();
			    			Estabelecimento est = estabelecimentoService.getByCNPJ(new Long(cnpjEstabelecimento));
			    			Estabelecimento estLote = est == null ? new Estabelecimento(nfce.getNFe().getInfNFe().getEmit()) : est;
			    			NotaFiscal n = new NotaFiscal(nfce, estLote);
			    			System.out.println(" ## IMPORTAR NF - CHAVE: " + nfce.getProtNFe().getInfProt().getChNFe());
			            salvarNFCeLote(n,estLote,getListaProdutosFromXML(nfce.getNFe().getInfNFe().getDet(), estLote, nfce));
		            } else if(versao(file.getInputStream()).equals(Uteis.VERSAO_4_00)) {
		            	InputStream content = file.getInputStream();
			            br.inf.portalfiscal.nfe.v_400.TNfeProc nfce = getNFeV4_00(content);
			            String cnpjEstabelecimento = nfce.getNFe().getInfNFe().getEmit().getCNPJ();
			    			Estabelecimento est = estabelecimentoService.getByCNPJ(new Long(cnpjEstabelecimento));
			    			Estabelecimento estLote = est == null ? new Estabelecimento(nfce.getNFe().getInfNFe().getEmit()) : est;
			    			NotaFiscal n = new NotaFiscal(nfce, estLote);
			    			System.out.println(" ## IMPORTAR NF - CHAVE: " + nfce.getProtNFe().getInfProt().getChNFe());
			            salvarNFCeLote(n,estLote,getListaProdutosFromXMLV4_00(nfce.getNFe().getInfNFe().getDet(), estLote, nfce));
		            }
		        	
		        }
		    }
			Uteis.MensagemInfo("Registros Salvo com sucesso!");
	    }catch(Exception e){
	    		System.out.println("Erro ao importar lista XML: " + e.getMessage() + e.getCause());
	    }
	}
	
	public void salvarNFCeLote(NotaFiscal nfceLote, Estabelecimento estabelecimentoLote, List<Produto> listaProdsLote) {		 
		try {
			if(estabelecimentoLote.getId() == null) estabelecimentoService.salvar(estabelecimentoLote);
			nfceLote.setXml(xml);
			nfceLote.setDtInclusao(new Date());
			if(this.usuario != null) nfceLote.setIdUsuario(this.usuario);
			notaFiscalservice.salvar(nfceLote);			
			atualizarProdutosLote(nfceLote, listaProdsLote);
			//limparReferencias();			
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			System.out.println("Nota Fiscal já salva: " + protocoloNf);
		} catch (NegocioException e) {
			e.printStackTrace();
		}catch (javax.persistence.PersistenceException e) {
			//e.printStackTrace();
			Uteis.MensagemAlerta("Nota Fiscal já foi cadastrada: " + nfceLote.getNumero());
		}		
	}
	
	
	private void visualizarNfe(TNfeProc nfe) {
		String cnpjEstabelecimento = nfe.getNFe().getInfNFe().getEmit().getCNPJ();
		Estabelecimento estabExiste = estabelecimentoService.getByCNPJ(new Long(cnpjEstabelecimento));
		estabelecimento = estabExiste == null ? new Estabelecimento(nfe.getNFe().getInfNFe().getEmit()) : estabExiste;
		notaFiscal = new NotaFiscal(nfe, estabelecimento);
		produtos = preencherListaProdutos(nfe.getNFe().getInfNFe().getDet(), estabelecimento, nfe);
		//Temp
		numeroNf = nfe.getNFe().getInfNFe().getIde().getCNF();
		protocoloNf = nfe.getProtNFe().getInfProt().getNProt();
		dataEmissaoNf = nfe.getNFe().getInfNFe().getIde().getDhEmi();
		localNf = nfe.getNFe().getInfNFe().getEmit().getXFant();
	}
	
	private void visualizarNfeV4_00(br.inf.portalfiscal.nfe.v_400.TNfeProc nfe) {
		String cnpjEstabelecimento = nfe.getNFe().getInfNFe().getEmit().getCNPJ();
		Estabelecimento estabExiste = estabelecimentoService.getByCNPJ(new Long(cnpjEstabelecimento));
		estabelecimento = estabExiste == null ? new Estabelecimento(nfe.getNFe().getInfNFe().getEmit()) : estabExiste;
		notaFiscal = new NotaFiscal(nfe, estabelecimento);
		produtos = preencherListaProdutosV4_00(nfe.getNFe().getInfNFe().getDet(), estabelecimento, nfe);
		//Temp
		numeroNf = nfe.getNFe().getInfNFe().getIde().getCNF();
		protocoloNf = nfe.getProtNFe().getInfProt().getNProt();
		dataEmissaoNf = nfe.getNFe().getInfNFe().getIde().getDhEmi();
		localNf = nfe.getNFe().getInfNFe().getEmit().getXFant();
	}
	
	public void validarArquivo(){
    	
    }
	
	public void salvarNFCe() {		 
		try {
			if(estabelecimento.getId() == null) estabelecimentoService.salvar(estabelecimento);
			notaFiscal.setXml(xml);			
			if(usuario != null) notaFiscal.setIdUsuario(usuario);
			notaFiscalservice.salvar(notaFiscal);			
			atualizarProdutos(notaFiscal);
			//limparReferencias();
			Uteis.MensagemInfo("Registro Salvo com sucesso!");
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			System.out.println("Nota Fiscal já salva: " + protocoloNf);
		} catch (NegocioException e) {
			e.printStackTrace();
		}catch (javax.persistence.PersistenceException e) {
			e.printStackTrace();
			Uteis.MensagemAlerta("Nota Fiscal já foi cadastrada: " + numeroNf);
		}		
	}
	
	private void limparReferencias(){
		this.arquivo = null;
		this.notaFiscal = null;
		this.estabelecimento = null;
		this.produtos.clear();
	}
	
	private void atualizarProdutos(NotaFiscal n){
		try {
			for(Produto p : produtos){
				p.setIdNotafiscal(n);
				produtoService.salvar(p);
			}
		} catch (Exception e) {
			System.out.println("Erro ao atualizar produtos: " + e.getMessage());
		}
	}
	
	private void atualizarProdutosLote(NotaFiscal n, List<Produto> ls){
		try {
			for(Produto p : ls){
				p.setIdNotafiscal(n);
				produtoService.salvar(p);
			}
		} catch (Exception e) {
			System.out.println("Erro ao atualizar produtos: " + e.getMessage());
		}
	}
	
	private List<Produto> preencherListaProdutos(List<Det> dets, Estabelecimento e, TNfeProc nfe){
		List<Produto> listaProdutos = new ArrayList<Produto>();
		for (Det det : dets) {
			listaProdutos.add(new Produto(det, e, nfe));
		}
		return listaProdutos;
	}
	
	private List<Produto> preencherListaProdutosV4_00(List<br.inf.portalfiscal.nfe.v_400.TNFe.InfNFe.Det> dets, Estabelecimento e, br.inf.portalfiscal.nfe.v_400.TNfeProc nfe){
		List<Produto> listaProdutos = new ArrayList<Produto>();
		for (br.inf.portalfiscal.nfe.v_400.TNFe.InfNFe.Det det : dets) {
			listaProdutos.add(new Produto(det, e, nfe));
		}
		return listaProdutos;
	}
	
	public static TNfeProc getNFeV3_10(String conteudo) throws Exception{        
        try {		
			JAXBContext context = JAXBContext.newInstance(TNfeProc.class);        
            Unmarshaller unmarshaller = context.createUnmarshaller();        
            TNfeProc nfe = unmarshaller.unmarshal(new StreamSource(new StringReader(conteudo)), TNfeProc.class).getValue();        
            return nfe;              
        } catch (JAXBException ex) {        
            throw new Exception(ex.toString());        
        }        
    }
	
	public static br.inf.portalfiscal.nfe.v_400.TNfeProc getNFeV4_00(String conteudo) throws Exception{        
        try {		
			JAXBContext context = JAXBContext.newInstance(br.inf.portalfiscal.nfe.v_400.TNfeProc.class);        
            Unmarshaller unmarshaller = context.createUnmarshaller();        
            br.inf.portalfiscal.nfe.v_400.TNfeProc nfe = unmarshaller.unmarshal(new StreamSource(new StringReader(conteudo)), br.inf.portalfiscal.nfe.v_400.TNfeProc.class).getValue();        
            return nfe;              
        } catch (JAXBException ex) {        
            throw new Exception(ex.toString());        
        }        
    }
	
	public static TNfeProc getNFe(InputStream in) throws Exception{        
        try {		
			Scanner scanner = new Scanner(in);
			String conteudo = scanner.useDelimiter("\\A").next();
			scanner.close();			
			JAXBContext context = JAXBContext.newInstance(TNfeProc.class);        
            Unmarshaller unmarshaller = context.createUnmarshaller();        
            TNfeProc nfe = unmarshaller.unmarshal(new StreamSource(new StringReader(conteudo)), TNfeProc.class).getValue();
            xml = conteudo.getBytes(Charset.forName("UTF-8"));
            return nfe;              
        } catch (JAXBException ex) {        
            throw new Exception(ex.toString());        
        }        
    }
	
	public static br.inf.portalfiscal.nfe.v_400.TNfeProc getNFeV4_00(InputStream in) throws Exception{        
        try {		
			Scanner scanner = new Scanner(in);
			String conteudo = scanner.useDelimiter("\\A").next();
			scanner.close();			
			JAXBContext context = JAXBContext.newInstance(br.inf.portalfiscal.nfe.v_400.TNfeProc.class);        
            Unmarshaller unmarshaller = context.createUnmarshaller();        
            br.inf.portalfiscal.nfe.v_400.TNfeProc nfe = unmarshaller.unmarshal(new StreamSource(new StringReader(conteudo)), br.inf.portalfiscal.nfe.v_400.TNfeProc.class).getValue();
            xml = conteudo.getBytes(Charset.forName("UTF-8"));
            return nfe;              
        } catch (JAXBException ex) {        
            throw new Exception(ex.toString());        
        }        
    }
	
	public void novaNotaFiscal(){
		limparReferencias();
	}
	
	//Getters and Setters
	public Part getArquivo() {
		return arquivo;
	}

	public void setArquivo(Part arquivo) {
		this.arquivo = arquivo;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public String getNumeroNf() {
		return numeroNf;
	}

	public void setNumeroNf(String numeroNf) {
		this.numeroNf = numeroNf;
	}

	public String getProtocoloNf() {
		return protocoloNf;
	}

	public void setProtocoloNf(String protocoloNf) {
		this.protocoloNf = protocoloNf;
	}

	public String getDataEmissaoNf() {
		return dataEmissaoNf;
	}

	public void setDataEmissaoNf(String dataEmissaoNf) {
		this.dataEmissaoNf = dataEmissaoNf;
	}

	public String getLocalNf() {
		return localNf;
	}

	public void setLocalNf(String localNf) {
		this.localNf = localNf;
	}

	public List<Part> getFiles() {
		return files;
	}

	public void setFiles(List<Part> files) {
		this.files = files;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	
}
