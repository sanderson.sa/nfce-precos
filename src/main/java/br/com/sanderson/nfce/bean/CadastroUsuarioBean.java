package br.com.sanderson.nfce.bean;

import java.io.Serializable;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.service.UsuarioService;
import br.com.sanderson.nfce.util.utilidades.Uteis;

@Model
@ViewScoped
public class CadastroUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Usuario usuario = new Usuario();
	
	@Inject
	private UsuarioService service;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public void registrar() {
		if(usuario.getNome() == null || usuario.getNome().isEmpty() && usuario.getNome().trim().length() < 5){
			Uteis.MensagemAtencao("O campo 'Nome' não pode ser vazio e precisa ter 10 caracteres no minímo.");
			return;
		}			
		else if(usuario.getEmail() == null || usuario.getEmail().isEmpty() || usuario.getEmail().trim().length() < 10){
			Uteis.MensagemAtencao("O campo 'E-mail' não pode ser vazio e precisa ter 10 caracteres no minímo.");
			return;
		}else if(usuario.getSenha() == null || usuario.getSenha().isEmpty() || usuario.getSenha().trim().length() < 5){
			Uteis.MensagemAtencao("Informe e confirme a 'Senha' com pelo menos 5 caracteres.");
			return;
		}else{
			try {
				usuario.setLogin(usuario.getEmail());
				service.salvar(usuario);
				//Uteis.MensagemInfo("Usuário registrado com sucesso, aguardo o recebimento de e-mail de verificação");
				Uteis.MensagemInfo("Registrado com sucesso, usuário " + "'" + usuario.getNome() + "'" +  " pronto para uso, faça login e cadastre suas NFCe");
			} catch (Exception e) {
				System.out.println(e.getMessage() + e.getCause());
			}
		}		
	}

}
