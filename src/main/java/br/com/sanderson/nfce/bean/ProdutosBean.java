package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.repository.ProdutoRepository;
import br.com.sanderson.nfce.service.ProdutoService;

@Model
@ViewScoped
public class ProdutosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Produto> produtos;
	private Produto produto;
	private String strProdutoPesquisa;
	private List<Produto> produtosAll;
	private List<String> listProdutos = new ArrayList<String>();;
	
	@Inject
	ProdutoService produtoService;
	
	private ProdutoRepository produtoRepository = new ProdutoRepository();
	
	@PostConstruct
	public void inicializar(){
		setProdutos(produtoService.produtos());
		listProdutos = produtosAll();
	}
	
	
	public List<String> produtosAll(){	
		if(listProdutos.isEmpty()) {
			produtosAll = produtoRepository.listarRest();
			System.out.println(" ##Lista Produto Rest");
			listProdutos.add("");
			for (int i = 0; i < produtosAll.size(); i++) {
				listProdutos.add(produtosAll.get(i).getCodigoProduto() + "-" + produtosAll.get(i).getDescricao());
			}
		}
		return listProdutos;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public String getStrProdutoPesquisa() {
		return strProdutoPesquisa;
	}

	public void setStrProdutoPesquisa(String strProdutoPesquisa) {
		this.strProdutoPesquisa = strProdutoPesquisa;
	}
	
	public String visualizarProduto() {
		String produtoCodigo = this.strProdutoPesquisa.split("-")[0];
		this.produto = produtoService.getByCodigoProduto(new BigInteger(produtoCodigo.trim())).get(0);
		return "produto-detalhe?faces-redirect=true&includeViewParams=true&id="+this.produto.getId();
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public List<String> getListProdutos() {
		return listProdutos;
	}


	public void setListProdutos(List<String> listProdutos) {
		this.listProdutos = listProdutos;
	}

}
