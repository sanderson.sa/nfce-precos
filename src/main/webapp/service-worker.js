var cacheName = 'ecompras.me';
var filesToCache = [
//	  './',
//	  './inicio.xhtml',
//	  '/laytou_base.xhtml',
//	  '/resources/css/animate.min.css',
//	  '/resources/css/bootstrap.min.css',
//	  '/resources/css/demo-pro.css',
//	  '/resources/css/maps_search_box.css',
	  '/javax.faces.resource/jquery-3.2.1.min.js.xhtml?ln=js',
	  '/javax.faces.resource/jquery-ui.min.js.xhtml?ln=js',
	  '/javax.faces.resource/perfect-scrollbar.min.js.xhtml?ln=js',
	  '/javax.faces.resource/bootstrap.min.js.xhtml?ln=js',
	  '/javax.faces.resource/moment.min.js.xhtml?ln=js',
	  '/javax.faces.resource/bootstrap3-typeahead.min.js.xhtml?ln=js',
	  '/javax.faces.resource/jquery.autocomplete.js.xhtml?ln=js',
	  '/javax.faces.resource/jquery.bootstrap.wizard.min.js.xhtml?ln=js',
	  '/javax.faces.resource/jquery.datatables.js.xhtml?ln=js',
	  '/javax.faces.resource/jquery.validate.min.js.xhtml?ln=js',
	  '/javax.faces.resource/jsf.js.xhtml?ln=javax.faces&stage=Development',
	  '/javax.faces.resource/nfce.js.xhtml?ln=js',
	  '/javax.faces.resource/paper-dashboard.js.xhtml?ln=js',
	  '/resources/css/paper-dashboard.css',
	  '/resources/css/bootstrap.min.css',
	  '/resources/css/maps_search_box.css',
	  '/resources/css/themify-icons.css',
//	  '/resources/fonts/themify.woff?-fvbane',
//	  '/javax.faces.resource/app.js.xhtml?ln=js'
	];


self.addEventListener('install', function(e) {
  //console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      //console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          //console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  /*
   * Fixes a corner case in which the app wasn't returning the latest data.
   * You can reproduce the corner case by commenting out the line below and
   * then doing the following steps: 1) load app for first time so that the
   * initial New York City data is shown 2) press the refresh button on the
   * app 3) go offline 4) reload the app. You expect to see the newer NYC
   * data, but you actually see the initial data. This happens because the
   * service worker is not yet activated. The code below essentially lets
   * you activate the service worker faster.
   */
  return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
  //console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});
