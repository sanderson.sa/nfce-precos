package br.com.sanderson.nfce.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.repository.ProdutoRepository;

@FacesConverter(forClass = Produto.class)
public class ProdutoConverter implements Converter {
	
	@Inject
	private ProdutoRepository repository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		Produto retorno = null;
		if (value != null && !"".equals(value)) {
			retorno = this.repository.getById(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Produto object = ((Produto) value);
			return object.getId() == null ? null : object.getId().toString();
		}
		return null;
	}

}
