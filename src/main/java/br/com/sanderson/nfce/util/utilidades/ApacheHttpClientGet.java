package br.com.sanderson.nfce.util.utilidades;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

public class ApacheHttpClientGet {

//	private static final String gLobalGoogleUrl="http://maps.google.com/maps/api/geocode/";
	private static final String GMAPS_API="https://maps.googleapis.com/maps/api/geocode/";
	private static final String API_KEY_GMAPS_JS = "AIzaSyA0o93us3LBtpIrbbCUsrVJtI389peew4A";
	
	//GET Method 
	private static String GET(String url) throws Exception {
	        String result = null;
	        InputStream inputStream = null;
	        try {
	        	// create HTTP Client
				HttpClient httpClient = HttpClientBuilder.create().build();
	 
				// Create new getRequest with below mentioned URL
				HttpGet getRequest = new HttpGet(url);
	 
				// Add additional header to getRequest which accepts application/xml data
				getRequest.addHeader("accept", "application/json");
	 
				// Execute your request and catch response
				HttpResponse response = httpClient.execute(getRequest);
	 
				// Check for HTTP response code: 200 = success
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
				}
				
		        inputStream = response.getEntity().getContent();
		        if (inputStream != null) {
		            result = getStringFromInputStream(inputStream);
		            //System.out.println("Result: " + "result\n" + result);
		        } 
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	 
	 private static String getStringFromInputStream(InputStream in) { 
	        try {		
				Scanner scanner = new Scanner(in);
				String conteudo = scanner.useDelimiter("\\A").next();
				scanner.close();        
	            return conteudo;         
	        } catch (Exception ex) {        
	            System.out.println(ex.getMessage());
	            return null;
	        }        
	    }
	 
//	 	@SuppressWarnings("deprecation")
//	    public static String getLatLng(String accessToken) throws Exception{
//	        String query=gLobalGoogleUrl+"json?address="+URLEncoder.encode(accessToken)+"&sensor=false";
//	        //System.out.println("GETGoogleGeocoder" + query+"");
//	        return GET(query);
//	    }
	 	
	 	//https://stackoverflow.com/questions/98449/how-to-convert-an-address-to-a-latitude-longitude
	 	@SuppressWarnings("deprecation")
	    public static String getLatLng(String endereco) throws Exception{	        
	 		String lngLat = null;
	 		String query = GMAPS_API + "json?address=" + URLEncoder.encode(endereco) + "&sensor=false?key=" + API_KEY_GMAPS_JS;
	        String result = GET(query);
	        JSONObject json = new JSONObject(result);
            JSONArray array = json.getJSONArray("results");
            if(array.length() == 0) {
            		System.out.println(" ## NÃO LOCALIZOU LAT LNG:");
            		System.out.println(" ## ENDEREÇO: " + endereco);
            }else {
	            	for (int i = 0; i < array.length(); i++) {
						JSONObject formatted_address = array.getJSONObject(i);
						JSONObject geometry = formatted_address.getJSONObject("geometry");
						JSONObject location = geometry.getJSONObject("location");
						
	//					System.out.println("End.: " + formatted_address.getString("formatted_address"));
	//					System.out.println("Lng: " + location.get("lng"));
	//					System.out.println("Lat: " + location.get("lat"));
						
						lngLat = location.get("lat") + "," + location.get("lng");
				  }
            }			  
			  return lngLat;
	    }
	
//	 	public static void main(String[] args) {
//		  try {
//			  //End.: Av. Visc. de Souza Franco, 776 - Reduto, Belém - PA, Brazil
//			  String result = getLatLng("Avenida Visconde de Souza Franco, Belém, 776");
//              JSONObject json = new JSONObject(result);
//              JSONArray array = json.getJSONArray("results");
//			  for (int i = 0; i < array.length(); i++) {
//					JSONObject formatted_address = array.getJSONObject(i);
//					JSONObject geometry = formatted_address.getJSONObject("geometry");
//					JSONObject location = geometry.getJSONObject("location");
//					System.out.println("End.: " + formatted_address.getString("formatted_address"));
//					System.out.println("Lng: " + location.get("lng"));
//					System.out.println("Lat: " + location.get("lat"));
//			  }
//			  //String result2 = getLatLng("W Main St, Bergenfield, NJ 07621");
//		  } catch (Exception e) {
//
//		  }
//
//		}
	
}