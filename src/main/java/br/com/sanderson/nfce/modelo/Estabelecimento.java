/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sanderson.nfce.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import br.com.sanderson.nfce.util.utilidades.ApacheHttpClientGet;
import br.inf.portalfiscal.nfe.TEnderEmi;
import br.inf.portalfiscal.nfe.TNFe.InfNFe.Emit;

/**
 *
 * @author sanderson
 */
@Entity
@Table(name = "estabelecimento", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estabelecimento.findAll", query = "SELECT e FROM Estabelecimento e"),
    @NamedQuery(name = "Estabelecimento.findById", query = "SELECT e FROM Estabelecimento e WHERE e.id = :id"),
    @NamedQuery(name = "Estabelecimento.findByCnpj", query = "SELECT e FROM Estabelecimento e WHERE e.cnpj = :cnpj"),
    @NamedQuery(name = "Estabelecimento.findByRazaoSocial", query = "SELECT e FROM Estabelecimento e WHERE e.razaoSocial = :razaoSocial"),
    @NamedQuery(name = "Estabelecimento.findByNomeFantasia", query = "SELECT e FROM Estabelecimento e WHERE e.nomeFantasia = :nomeFantasia")})
public class Estabelecimento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "cnpj")
    private long cnpj;
    @Basic(optional = false)
    @Column(name = "razao_social")
    private String razaoSocial;
    @Basic(optional = false)
    @Column(name = "nome_fantasia")
    private String nomeFantasia;
    @Column(name = "latlng")
    private String latlng;
    @Column(name = "telefone")
    private String telefone;
    @OneToMany(mappedBy = "idEstabelecimento", cascade = CascadeType.ALL)
    private List<NotaFiscal> notaFiscalList;
    @OneToMany(mappedBy = "idEstabelecimento", fetch=FetchType.EAGER)
    private List<Produto> produtoList;
    @JoinColumn(name = "id_endereco", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Endereco idEndereco;

    public Estabelecimento() {
    }

    public Estabelecimento(Long id) {
        this.id = id;
    }
    
    public Estabelecimento(Emit emitente) {
    	if(emitente != null){
    		this.cnpj = emitente.getCNPJ() != null ? new Long(emitente.getCNPJ()) : new Long("0");
        	this.nomeFantasia = emitente.getXNome() != null ? emitente.getXNome() : "Não identificado";
        	this.razaoSocial = emitente.getXNome() != null ? emitente.getXNome() : "Não identificado";
        	this.idEndereco = emitente.getEnderEmit() != null ? new Endereco(emitente.getEnderEmit()) : null;        	
        	this.telefone = emitente.getEnderEmit().getFone() != null ? emitente.getEnderEmit().getFone() : "";
        	this.latlng = emitente.getEnderEmit() != null ? setLatLng(emitente.getEnderEmit()) : null;
    	}else{
    		System.out.println(" ## INFO ## Emissor não identificado.");
    	}    	
    }
    
    public Estabelecimento(br.inf.portalfiscal.nfe.v_400.TNFe.InfNFe.Emit emitente) {
    	if(emitente != null){
    		this.cnpj = emitente.getCNPJ() != null ? new Long(emitente.getCNPJ()) : new Long("0");
        	this.nomeFantasia = emitente.getXNome() != null ? emitente.getXNome() : "Não identificado";
        	this.razaoSocial = emitente.getXNome() != null ? emitente.getXNome() : "Não identificado";
        	this.idEndereco = emitente.getEnderEmit() != null ? new Endereco(emitente.getEnderEmit()) : null;        	
        	this.telefone = emitente.getEnderEmit().getFone() != null ? emitente.getEnderEmit().getFone() : "";
        	this.latlng = emitente.getEnderEmit() != null ? setLatLng(emitente.getEnderEmit()) : null;
    	}else{
    		System.out.println(" ## INFO ## Emissor não identificado.");
    	}    	
    }
    
    private String setLatLng(TEnderEmi enderecoEmitente){
    	try{
    		String endereco = enderecoEmitente.getXLgr() + ","
        			+ enderecoEmitente.getNro() + ","
        			+ enderecoEmitente.getXBairro() + ","
        			+ enderecoEmitente.getXMun();
        	return ApacheHttpClientGet.getLatLng(endereco);
    	}catch(Exception e){
    		return null;
    	}    	
    }
    
    private String setLatLng(br.inf.portalfiscal.nfe.v_400.TEnderEmi enderecoEmitente){
    	try{
    		String endereco = enderecoEmitente.getXLgr() + ","
        			+ enderecoEmitente.getNro() + ","
        			+ enderecoEmitente.getXBairro() + ","
        			+ enderecoEmitente.getXMun();
        	return ApacheHttpClientGet.getLatLng(endereco);
    	}catch(Exception e){
    		return null;
    	}    	
    }

    public Estabelecimento(Long id, long cnpj, String razaoSocial, String nomeFantasia) {
        this.id = id;
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCnpj() {
        return cnpj;
    }

    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    @XmlTransient
    public List<NotaFiscal> getNotaFiscalList() {
        return notaFiscalList;
    }

    public void setNotaFiscalList(List<NotaFiscal> notaFiscalList) {
        this.notaFiscalList = notaFiscalList;
    }
    
    @XmlTransient
    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }

    public Endereco getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(Endereco idEndereco) {
        this.idEndereco = idEndereco;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estabelecimento)) {
            return false;
        }
        Estabelecimento other = (Estabelecimento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return cnpj + " - " + nomeFantasia;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
}
