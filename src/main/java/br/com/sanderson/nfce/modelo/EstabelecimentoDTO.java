package br.com.sanderson.nfce.modelo;

import java.math.BigDecimal;

public class EstabelecimentoDTO {
	private Long idEstabelecimento;
	private String noEstabelecimento;
	private Integer nuNotas;
	private BigDecimal vlTotalEstabelecimento;
	
	/**
	 * 147	Saraiva e Siciliano SA	2	127.29
	 * @param object
	 * @param object2
	 * @param object3
	 * @param object4
	 */
	public EstabelecimentoDTO(Object id, Object noEstab, Object qtdNotas, Object vlTotalEstab) {
		idEstabelecimento = id != null ? new Long(id.toString()) : null;
		noEstabelecimento = noEstab != null ? noEstab.toString() : null;
		nuNotas = qtdNotas != null ? new Integer(qtdNotas.toString()) : 0;
		vlTotalEstabelecimento = vlTotalEstab != null ? new BigDecimal(vlTotalEstab.toString()) : new BigDecimal(0);
	}
	public String getNoEstabelecimento() {
		return noEstabelecimento;
	}
	public void setNoEstabelecimento(String noEstabelecimento) {
		this.noEstabelecimento = noEstabelecimento;
	}
	public Integer getNuNotas() {
		return nuNotas;
	}
	public void setNuNotas(Integer nuNotas) {
		this.nuNotas = nuNotas;
	}
	public BigDecimal getVlTotalEstabelecimento() {
		return vlTotalEstabelecimento;
	}
	public void setVlTotalEstabelecimento(BigDecimal vlTotalEstabelecimento) {
		this.vlTotalEstabelecimento = vlTotalEstabelecimento;
	}
	public Long getIdEstabelecimento() {
		return idEstabelecimento;
	}
	public void setIdEstabelecimento(Long idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}
}
