/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sanderson.nfce.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import br.inf.portalfiscal.nfe.TEnderEmi;

/**
 *
 * @author sanderson
 */
@Entity
@Table(name = "endereco", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Endereco.findAll", query = "SELECT e FROM Endereco e"),
    @NamedQuery(name = "Endereco.findById", query = "SELECT e FROM Endereco e WHERE e.id = :id"),
    @NamedQuery(name = "Endereco.findByCep", query = "SELECT e FROM Endereco e WHERE e.cep = :cep"),
    @NamedQuery(name = "Endereco.findByBairro", query = "SELECT e FROM Endereco e WHERE e.bairro = :bairro"),
    @NamedQuery(name = "Endereco.findByLogradouro", query = "SELECT e FROM Endereco e WHERE e.logradouro = :logradouro"),
    @NamedQuery(name = "Endereco.findByNumero", query = "SELECT e FROM Endereco e WHERE e.numero = :numero"),
    @NamedQuery(name = "Endereco.findByComplemento", query = "SELECT e FROM Endereco e WHERE e.complemento = :complemento"),
    @NamedQuery(name = "Endereco.findByCidade", query = "SELECT e FROM Endereco e WHERE e.cidade = :cidade"),
    @NamedQuery(name = "Endereco.findByUf", query = "SELECT e FROM Endereco e WHERE e.uf = :uf"),
    @NamedQuery(name = "Endereco.findByCodCidade", query = "SELECT e FROM Endereco e WHERE e.codCidade = :codCidade")})
public class Endereco implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "cep")
    private String cep;
    @Column(name = "bairro")
    private String bairro;
    @Column(name = "logradouro")
    private String logradouro;
    @Column(name = "numero")
    private String numero;
    @Column(name = "complemento")
    private String complemento;
    @Column(name = "cidade")
    private String cidade;
    @Column(name = "uf")
    private String uf;
    @Column(name = "cod_cidade")
    private String codCidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEndereco")
    private List<Estabelecimento> estabelecimentoList;

    public Endereco() {
    }

    public Endereco(Long id) {
        this.id = id;
    }
    
    public Endereco(TEnderEmi endereco){
    	this.cep = endereco.getCEP();
    	this.bairro = endereco.getXBairro();
    	this.cidade = endereco.getXMun();
    	this.codCidade = endereco.getCMun();
    	this.logradouro = endereco.getXLgr();
    	this.numero = endereco.getNro();
    	//this.uf = endereco.getUF().getDeclaringClass().getName()
    }
    
    public Endereco(br.inf.portalfiscal.nfe.v_400.TEnderEmi endereco){
    	this.cep = endereco.getCEP();
    	this.bairro = endereco.getXBairro();
    	this.cidade = endereco.getXMun();
    	this.codCidade = endereco.getCMun();
    	this.logradouro = endereco.getXLgr();
    	this.numero = endereco.getNro();
    	//this.uf = endereco.getUF().getDeclaringClass().getName()
    }

    public Endereco(Long id, String cep) {
        this.id = id;
        this.cep = cep;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCodCidade() {
        return codCidade;
    }

    public void setCodCidade(String codCidade) {
        this.codCidade = codCidade;
    }

    @XmlTransient
    public List<Estabelecimento> getEstabelecimentoList() {
        return estabelecimentoList;
    }

    public void setEstabelecimentoList(List<Estabelecimento> estabelecimentoList) {
        this.estabelecimentoList = estabelecimentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Endereco)) {
            return false;
        }
        Endereco other = (Endereco) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String enderecoCompleto() {
    		String endCompleto = logradouro + ", " + numero + " - " + bairro + " - " + cep;
    		return endCompleto;
    }
    
    @Override
    public String toString() {
        return "br.com.sanderson.nfce.modelo.Endereco[ id=" + id + " ]";
    }
    
}
