package br.com.sanderson.nfce.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.repository.EstabelecimentoRepository;

@FacesConverter(forClass = Estabelecimento.class)
public class EstabelecimentoConverter implements Converter {
	
	
	@Inject
	private EstabelecimentoRepository repository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		Estabelecimento retorno = null;
		if (value != null && !"".equals(value)) {
			retorno = this.repository.getById(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Estabelecimento object = ((Estabelecimento) value);
			return object.getId() == null ? null : object.getId().toString();
		}
		return null;
	}

}
