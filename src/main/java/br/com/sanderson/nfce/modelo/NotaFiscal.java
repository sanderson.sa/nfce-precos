/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sanderson.nfce.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import br.com.sanderson.nfce.util.utilidades.Uteis;
import br.inf.portalfiscal.nfe.TNfeProc;
import br.inf.portalfiscal.nfe.v_400.TNFe;

/**
 *
 * @author sanderson
 */
@Entity
@Table(name = "nota_fiscal", schema = "public")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "NotaFiscal.findAll", query = "SELECT n FROM NotaFiscal n"),
    @NamedQuery(name = "NotaFiscal.findById", query = "SELECT n FROM NotaFiscal n WHERE n.id = :id"),
    @NamedQuery(name = "NotaFiscal.findByDataEmissao", query = "SELECT n FROM NotaFiscal n WHERE n.dataEmissao = :dataEmissao"),
    @NamedQuery(name = "NotaFiscal.findByDtInclusao", query = "SELECT n FROM NotaFiscal n WHERE n.dtInclusao = :dtInclusao"),
    @NamedQuery(name = "NotaFiscal.findByUsuario", query = "SELECT n FROM NotaFiscal n WHERE n.idUsuario = :idUsuario ORDER BY n.dataEmissao DESC"),
    @NamedQuery(name = "NotaFiscal.findByUsuarioAndEstab", query = "SELECT n FROM NotaFiscal n WHERE n.idUsuario = :idUsuario AND n.idEstabelecimento = :idEstab ORDER BY n.dataEmissao DESC"),
    @NamedQuery(name = "NotaFiscal.findByCpfNf", query = "SELECT n FROM NotaFiscal n WHERE n.cpfNf = :cpfNf")})
public class NotaFiscal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "numero")
    private String numero;
    @Basic(optional = false)
    @Column(name = "chave")
    private String chave;
    @Basic(optional = false)
    @Column(name = "data_emissao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataEmissao;
    @Column(name = "dt_inclusao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtInclusao;
    //@Lob
    @Column(name = "xml")
    private byte[] xml;
    @Column(name = "cpf_nf")
    private BigInteger cpfNf;
 // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_nfce")
    private BigDecimal vlNfce;
    @Column(name = "itens")
    private Integer itens;
    @JoinColumn(name = "id_estabelecimento", referencedColumnName = "id")    
    @ManyToOne
    private Estabelecimento idEstabelecimento;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne
    private Usuario idUsuario;
    @OneToMany(mappedBy = "idNotafiscal")
    private List<Produto> produtoList;

    public NotaFiscal() {
    }

    public NotaFiscal(Long id) {
        this.id = id;
    }
    
    public NotaFiscal (TNfeProc nfe, Estabelecimento estabelecimento){
    	this.chave = nfe.getProtNFe().getInfProt().getChNFe();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	try {
			this.dataEmissao = formatter.parse(nfe.getNFe().getInfNFe().getIde().getDhEmi().substring(0, 20));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	this.dtInclusao = new Date();
    	this.idEstabelecimento = estabelecimento;
    	this.cpfNf = new BigInteger(nfe.getNFe().getInfNFe().getDest().getCPF());
    	this.vlNfce = new BigDecimal(nfe.getNFe().getInfNFe().getPag().get(0).getVPag().replaceAll(",", "."));    	    	
    	this.itens = nfe.getNFe().getInfNFe().getDet().size(); // numero de itens
    	this.numero = nfe.getNFe().getInfNFe().getIde().getCNF();
    }
    
    public NotaFiscal (br.inf.portalfiscal.nfe.v_400.TNfeProc nfe, Estabelecimento estabelecimento){
    	this.chave = nfe.getProtNFe().getInfProt().getChNFe();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	try {
			this.dataEmissao = formatter.parse(nfe.getNFe().getInfNFe().getIde().getDhEmi().substring(0, 20));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	this.dtInclusao = new Date();
    	this.idEstabelecimento = estabelecimento;
    	this.cpfNf = new BigInteger(nfe.getNFe().getInfNFe().getDest().getCPF());
    	//varrer lista de formas de pagamento
    	BigDecimal vTotal = BigDecimal.ZERO;
    	for(TNFe.InfNFe.Pag.DetPag item : nfe.getNFe().getInfNFe().getPag().getDetPag()) {
    		vTotal = vTotal.add(new BigDecimal(item.getVPag()));
    	}
    	if(nfe.getNFe().getInfNFe().getPag().getVTroco() != null
    			&& !nfe.getNFe().getInfNFe().getPag().getVTroco().isEmpty()) {
    		this.vlNfce = vTotal.subtract(new BigDecimal(nfe.getNFe().getInfNFe().getPag().getVTroco()));
    	}else {
    		this.vlNfce = vTotal;
    	}
    	    	    	
    	this.itens = nfe.getNFe().getInfNFe().getDet().size(); // numero de itens
    	this.numero = nfe.getNFe().getInfNFe().getIde().getCNF();
    }

    public NotaFiscal(Long id, String chave, Date dataEmissao) {
        this.id = id;
        this.chave = chave;
        this.dataEmissao = dataEmissao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public Date getDtInclusao() {
        return dtInclusao;
    }

    public void setDtInclusao(Date dtInclusao) {
        this.dtInclusao = dtInclusao;
    }

    public byte[] getXml() {
        return xml;
    }

    public void setXml(byte[] xml) {
        this.xml = xml;
    }

    public BigInteger getCpfNf() {
        return cpfNf;
    }

    public void setCpfNf(BigInteger cpfNf) {
        this.cpfNf = cpfNf;
    }
    
    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }
    
    public String getDataFormatada(){
    	return Uteis.dataFormatada("dd-MM-yyyy HH:mm:ss", dataEmissao);
    }

    public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Estabelecimento getIdEstabelecimento() {
        return idEstabelecimento;
    }

    public void setIdEstabelecimento(Estabelecimento idEstabelecimento) {
        this.idEstabelecimento = idEstabelecimento;
    }

    @XmlTransient
    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }
    
    public String getDtCompra() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(dataEmissao);
	}
    
    public BigDecimal getVlNfce() {
		return vlNfce;
	}

	public void setVlNfce(BigDecimal vlNfce) {
		this.vlNfce = vlNfce;
	}

	public Integer getItens() {
		return itens;
	}

	public void setItens(Integer itens) {
		this.itens = itens;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaFiscal)) {
            return false;
        }
        NotaFiscal other = (NotaFiscal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return numero + " - " +  dataEmissao.toString() + " - " + idEstabelecimento.getNomeFantasia();
    }
    
}
