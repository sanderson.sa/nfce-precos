package br.com.sanderson.nfce.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.ListaCompra;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.modelo.repository.ListaComprasRepository;
import br.com.sanderson.nfce.util.jpa.Transactional;

public class ListaCompraService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	ListaComprasRepository repository;
	
	@Transactional
	public void salvar(ListaCompra item) throws NegocioException {
		//Regra de negócio aqui.
		
		//Salvar
		this.repository.adicionar(item);
	}
	
	public List<ListaCompra> listar(){
		return repository.listar();
	}
	
	public List<ListaCompra> findByUsuario(Usuario u){
		return repository.findByUsuario(u);
	}
	
	@Transactional
	public void atualizar(ListaCompra item) throws NegocioException {
		// Regra de negócio aqui.

		// Atualizar
		this.repository.editar(item);
	}

}
