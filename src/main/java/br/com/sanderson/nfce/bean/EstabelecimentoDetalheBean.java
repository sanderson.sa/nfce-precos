package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.service.ProdutoService;


@Model
@ViewScoped
public class EstabelecimentoDetalheBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Estabelecimento estabelecimento = new Estabelecimento();
	
	@Inject
	private ProdutoService produtoService;	
	
	private List<Produto> listaProdutosEstabelecimento;
	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public ProdutoService getProdutoService() {
		return produtoService;
	}
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}
	public List<Produto> getListaProdutosEstabelecimento() {
		return listaProdutosEstabelecimento;
	}
	public void setListaProdutosEstabelecimento(
			List<Produto> listaProdutosEstabelecimento) {
		this.listaProdutosEstabelecimento = listaProdutosEstabelecimento;
	}
	
	public void inicializarEstabelecimentoDetalhe(){
		listaProdutosEstabelecimento = produtoService.getByEstabelecimento(estabelecimento.getId());
	}
	

}
