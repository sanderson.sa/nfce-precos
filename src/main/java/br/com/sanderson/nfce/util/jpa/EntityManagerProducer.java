package br.com.sanderson.nfce.util.jpa;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@ApplicationScoped
public class EntityManagerProducer {

	private EntityManagerFactory factory;
	
    private PersistenceProperties properties = new PersistenceProperties();

	public EntityManagerProducer() {
		//Heroku
//		this.factory = Persistence.createEntityManagerFactory("bemagora_pu", properties.get());
		
		//Local
		this.factory = Persistence.createEntityManagerFactory("nfce_pu");
	}

	@Produces
	@RequestScoped
	public EntityManager createEntityManager() {
		return factory.createEntityManager();
	}

	public void closeEntityManager(@Disposes EntityManager manager) {
		manager.close();
	}

}