package br.com.sanderson.nfce.modelo.repository;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.util.jpa.JpaUtil;

public class ProdutoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	
	public List<Produto> listar() {
		TypedQuery<Produto> query = manager.createQuery("from Produto", Produto.class);
		List<Produto> listaProdutos = (List<Produto>) query.getResultList();
		//https://stackoverflow.com/questions/29670116/remove-duplicates-from-a-list-of-objects-based-on-property-in-java-8
		//List<Employee> unique = employee.stream().collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingInt(Employee::getId))),ArrayList::new));
		Collections.sort(listaProdutos, (p1, p2) -> p2.getIdNotafiscal().getDataEmissao().compareTo(p1.getIdNotafiscal().getDataEmissao()));
		List<Produto> listaDistinct = listaProdutos.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Produto::getCodigoProduto)))).stream().collect(Collectors.toList());
		Collections.sort(listaDistinct, (p1, p2) -> p2.getIdNotafiscal().getDataEmissao().compareTo(p1.getIdNotafiscal().getDataEmissao()));
		return listaDistinct.subList(0, 50);
	}
	
	/**
	 * Métodos chamados da classe ServiceController não são injetados
	 * @return
	 */
	public List<Produto> listarRest() {
		TypedQuery<Produto> query = JpaUtil.getEntityManager().createQuery("from Produto", Produto.class);
		List<Produto> listaProdutos = (List<Produto>) query.getResultList();		
		List<Produto> listaDistintos = listaProdutos.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Produto::getCodigoProduto)))).stream().collect(Collectors.toList());
		return listaDistintos;
		
		
	}
	
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<TipoProcessoDespachoPadrao> buscarTipoProcessoDespachoPadraoPorTipoProcesso(TipoProcesso tipoProcesso) throws DAOException {
//		String sql = "select tpdp.* from protocolo.tipo_processo_despacho_padrao tpdp "
//					 + "where tpdp.id_tp_processo = :tipoProcesso";
//		Query q = tipoProcessoDespachoPadraoDAO.getEntityManager().createNativeQuery(sql, TipoProcessoDespachoPadrao.class);
//		q.setParameter("tipoProcesso", tipoProcesso.getIdTpProcesso());
//		return q.getResultList();
//	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> getByCodigoProduto(BigInteger codigoProduto) {
		//select p.*, n.data_emissao, n.id_estabelecimento from produto p join nota_fiscal n on p.id_notafiscal = n."id" where p.codigo_produto = 18 order by n.data_emissao desc;		
		String sql = "select p.*, n.data_emissao from produto p join nota_fiscal n on p.id_notafiscal = n.id where p.codigo_produto = :codigoProduto order by n.data_emissao desc";
		Query q = manager.createNativeQuery(sql, Produto.class);
		q.setParameter("codigoProduto", codigoProduto);
		List<Produto> produtos = (List<Produto>) q.getResultList();
		List<Produto> listaDistinct = produtos.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Produto::getHash)))).stream().collect(Collectors.toList());
		//listaDistinct.forEach((produto)->System.out.println(produto.getDescricao()));

		//https://dzone.com/articles/using-lambda-expression-sort
		//https://stackoverflow.com/questions/28607191/how-to-use-a-java8-lambda-to-sort-a-stream-in-reverse-order
		//https://stackoverflow.com/questions/18073590/sort-list-in-reverse-order
		Collections.sort(listaDistinct, (p1, p2) -> p2.getIdNotafiscal().getDataEmissao().compareTo(p1.getIdNotafiscal().getDataEmissao()));
		return listaDistinct;
	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> getByEstabelecimento(Long id) {
		//select p.*, n.data_emissao, n.id_estabelecimento from produto p join nota_fiscal n on p.id_notafiscal = n."id" where p.codigo_produto = 18 order by n.data_emissao desc;		
		String sql = "select p.*, n.data_emissao from produto p  join nota_fiscal n on p.id_notafiscal = n.id join estabelecimento e on n.id_estabelecimento = e.id where e.id = :id order by n.data_emissao desc";
		Query q = manager.createNativeQuery(sql, Produto.class);
		q.setParameter("id", id);
		List<Produto> produtos = (List<Produto>) q.getResultList();
		List<Produto> listaDistinct = produtos.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Produto::getCodigoProduto)))).stream().collect(Collectors.toList());
		//listaDistinct.forEach((produto)->System.out.println(produto.getDescricao()));

		//https://dzone.com/articles/using-lambda-expression-sort
		//https://stackoverflow.com/questions/28607191/how-to-use-a-java8-lambda-to-sort-a-stream-in-reverse-order
		//https://stackoverflow.com/questions/18073590/sort-list-in-reverse-order
		Collections.sort(listaDistinct, (p1, p2) -> p2.getIdNotafiscal().getDataEmissao().compareTo(p1.getIdNotafiscal().getDataEmissao()));
		return listaDistinct;
	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> getByEstabelecimentoAndProduto(Long id, BigInteger codigoProduto) {
		//select p.*, n.data_emissao, n.id_estabelecimento from produto p join nota_fiscal n on p.id_notafiscal = n."id" where p.codigo_produto = 18 order by n.data_emissao desc;		
		String sql = "select p.*, n.data_emissao from produto p join nota_fiscal n on p.id_notafiscal = n.id join estabelecimento e on n.id_estabelecimento = e.id where e.id = :id and p.codigo_produto = :codigoProduto order by n.data_emissao desc";
		Query q = manager.createNativeQuery(sql, Produto.class);
		q.setParameter("id", id);
		q.setParameter("codigoProduto", codigoProduto);
		List<Produto> produtos = (List<Produto>) q.getResultList();
		List<Produto> listaDistinct = produtos.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Produto::getHash)))).stream().collect(Collectors.toList());
		//listaDistinct.forEach((produto)->System.out.println(produto.getDescricao()));
		
		//https://dzone.com/articles/using-lambda-expression-sort
		//https://stackoverflow.com/questions/28607191/how-to-use-a-java8-lambda-to-sort-a-stream-in-reverse-order
		//https://stackoverflow.com/questions/18073590/sort-list-in-reverse-order
		Collections.sort(listaDistinct, (p1, p2) -> p2.getIdNotafiscal().getDataEmissao().compareTo(p1.getIdNotafiscal().getDataEmissao()));
		return listaDistinct;
	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> getByEstabelecimentoAndProdutoListaCompra(Long id, BigInteger codigoProduto) {
		//select p.*, n.data_emissao, n.id_estabelecimento from produto p join nota_fiscal n on p.id_notafiscal = n."id" where p.codigo_produto = 18 order by n.data_emissao desc;		
		String sql = "select p.*, n.data_emissao from produto p join nota_fiscal n on p.id_notafiscal = n.id join estabelecimento e on n.id_estabelecimento = e.id where e.id = :id and p.codigo_produto = :codigoProduto order by n.data_emissao desc";
		Query q = manager.createNativeQuery(sql, Produto.class);
		q.setParameter("id", id);
		q.setParameter("codigoProduto", codigoProduto);
		List<Produto> produtos = (List<Produto>) q.getResultList();
		List<Produto> listaDistinct = produtos.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Produto::getHash)))).stream().collect(Collectors.toList());
		//listaDistinct.forEach((produto)->System.out.println(produto.getDescricao()));
		
		//https://dzone.com/articles/using-lambda-expression-sort
		//https://stackoverflow.com/questions/28607191/how-to-use-a-java8-lambda-to-sort-a-stream-in-reverse-order
		//https://stackoverflow.com/questions/18073590/sort-list-in-reverse-order
		Collections.sort(listaDistinct, (p1, p2) -> p2.getIdNotafiscal().getDataEmissao().compareTo(p1.getIdNotafiscal().getDataEmissao()));
		return listaDistinct;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Produto> getByNotaFiscal(Long id) {
		//select p.*, n.data_emissao, n.id_estabelecimento from produto p join nota_fiscal n on p.id_notafiscal = n."id" where p.codigo_produto = 18 order by n.data_emissao desc;		
		String sql = "select p.*, n.data_emissao from produto p join nota_fiscal n on p.id_notafiscal = n.id where n.id = :id order by p.codigo_produto desc";
		Query q = manager.createNativeQuery(sql, Produto.class);
		q.setParameter("id", id);
		List<Produto> produtos = (List<Produto>) q.getResultList();
		return produtos;
	}
	
	
	public Produto findByDescricao(String descricao) {
		Query query = manager.createNamedQuery("Produto.findByDescricao");
		query.setParameter("descricao", descricao);
		try {
			return (Produto) query.getResultList().get(0);
		} catch (Exception e) {
			return null;
		}		
	}
	
	public Produto getById(Long id) {
		return manager.find(Produto.class, id);
	}
	
	public void adicionar(Produto p) throws PersistenceException {		
		manager.persist(p);		
	}
	
	public void guardar(Produto p) throws PersistenceException {		
		manager.merge(p);		
	}
	
	public void editar(Produto p) {
		manager.merge(p);
	}
	

}
