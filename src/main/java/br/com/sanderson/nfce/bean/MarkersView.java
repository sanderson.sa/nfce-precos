package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.service.EstabelecimentoService;
 
@Model
@ViewScoped
public class MarkersView implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MapModel simpleModel;
	private Marker marker;
	
	@Inject
	EstabelecimentoService estabelecimentoService;
  
    @PostConstruct
    public void init() {
    	
        simpleModel = new DefaultMapModel();
          
        List<Estabelecimento> listaEstabelecimentos = estabelecimentoService.estabelecimentos();
        for (Estabelecimento e : listaEstabelecimentos) {
        	if(e.getLatlng() != null) {
	        	String[] coordenadas = e.getLatlng().split(",");
	        	double lat = new Double(coordenadas[0]);
	        	double lng = new Double(coordenadas[1]);
	        	LatLng coordenada = new LatLng(lat,lng);
	        	System.out.println(" ## Adicionar Estabelecimentos: " + e.getCnpj() + e.getNomeFantasia() + lat+","+lng);
	        	simpleModel.addOverlay(new Marker(coordenada, e.getNomeFantasia()));
        	}
		}
        
        //Basic marker
      //LatLng coord1 = new LatLng(-1.4463307,-48.4897426);
        //simpleModel.addOverlay(new Marker(coord1, "Av. Sen. Lemos, 3153 - Sacramenta, Belém - PA, 66120-000, Brazil"));
      
    }
  
    public MapModel getSimpleModel() {
        return simpleModel;
    }
    //http://www.guj.com.br/t/infowindow-do-gmap-primefaces-nao-funciona-resolvido/192596
    public void onMarkerSelect(OverlaySelectEvent event) {
        this.marker = (Marker) event.getOverlay();
    }
    
    public Marker getMarker() {
        return marker;
    }
    
    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
