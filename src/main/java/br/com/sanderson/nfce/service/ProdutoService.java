package br.com.sanderson.nfce.service;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.repository.ProdutoRepository;
import br.com.sanderson.nfce.util.jpa.Transactional;

public class ProdutoService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	ProdutoRepository produtoRepository;
	
	public List<Produto> produtos(){
		return produtoRepository.listar();
	}
	
	public List<Produto> getByCodigoProduto(BigInteger codigoProduto) {
		return produtoRepository.getByCodigoProduto(codigoProduto);
	}
	
	public List<Produto> getByNotaFiscal(Long id) {
		return produtoRepository.getByNotaFiscal(id);
	}
	
	public List<Produto> getByEstabelecimento(Long id){
		return produtoRepository.getByEstabelecimento(id);
	}
	
	public List<Produto> getByEstabelecimentoAndProduto(Long id, BigInteger codigoProduto){
		return produtoRepository.getByEstabelecimentoAndProduto(id, codigoProduto);
	}
	
	@Transactional
	public void salvar(Produto produto) throws NegocioException {
		//Regra de negócio aqui.
		
		//Salvar
		this.produtoRepository.adicionar(produto);
	}
	
	@Transactional
	public void atualizar(Produto produto) throws NegocioException {
		// Regra de negócio aqui.

		// Atualizar
		this.produtoRepository.editar(produto);
	}
	
	

}
