package br.com.sanderson.nfce.bean;

import java.io.Serializable;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.service.NegocioException;
import br.com.sanderson.nfce.service.UsuarioService;
import br.com.sanderson.nfce.util.utilidades.Uteis;

@Model
@ViewScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private Usuario usuario = new Usuario();
	
	@Inject
	UsuarioService service;
	
	public void atualizarPerfil(){
		//Validar Perfil
		
		//Atualizar Perfil
		try {
			service.atualizar(usuario);
			Uteis.MensagemInfo("Perfil Atualizado com sucesso!");
		} catch (NegocioException e) {
			Uteis.MensagemAlerta("Erro ao atualizar Perfil, tente novamente.");
			e.printStackTrace();
		}
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	

}
