package br.com.sanderson.nfce.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.modelo.repository.UsuarioRepository;

@FacesConverter(forClass = Usuario.class)
public class UsuarioConverter implements Converter {
	
	
	
	@Inject
	private UsuarioRepository repository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		Usuario retorno = null;
		if (value != null && !"".equals(value)) {
			retorno = this.repository.getById(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Usuario object = ((Usuario) value);
			return object.getId() == null ? null : object.getId().toString();
		}
		return null;
	}

}
