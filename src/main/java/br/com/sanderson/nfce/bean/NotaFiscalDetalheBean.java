package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sanderson.nfce.modelo.NotaFiscal;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.service.ProdutoService;

@Model
@ViewScoped
public class NotaFiscalDetalheBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	
	private NotaFiscal notaFiscal = new NotaFiscal();
	
	@Inject
	ProdutoService produtoService;
	
	private List<Produto> produtos;
	
	public void inicializar(){
		setProdutos(produtoService.getByNotaFiscal(notaFiscal.getId()));
	}
	
	//Getters and Setters
	
	public List<Produto> getProdutos() {
		return produtos;
	}
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

}
