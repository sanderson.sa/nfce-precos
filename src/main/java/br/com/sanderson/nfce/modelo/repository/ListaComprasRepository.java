package br.com.sanderson.nfce.modelo.repository;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.sanderson.nfce.modelo.ListaCompra;
import br.com.sanderson.nfce.modelo.Usuario;

public class ListaComprasRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public List<ListaCompra> listar() {
		TypedQuery<ListaCompra> query = manager.createQuery("from ListaCompra", ListaCompra.class);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaCompra> findByUsuario(Usuario idUsuario) {
		Query query = manager.createNamedQuery("ListaCompra.findByUsuario");
		query.setParameter("idUsuario", idUsuario);
		List<ListaCompra> lista = (List<ListaCompra>) query.getResultList();
		return lista;
	}
	
	public ListaCompra getById(Long id) {
		return manager.find(ListaCompra.class, id);
	}
	
	public void adicionar(ListaCompra obj) throws PersistenceException {		
		manager.persist(obj);
		manager.flush();
	}
	
	public void guardar(ListaCompra obj) throws PersistenceException {		
		manager.merge(obj);		
	}
	
	public void editar(ListaCompra obj) {
		manager.merge(obj);
	}
	

}
