package br.com.sanderson.nfce.modelo.repository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import br.com.sanderson.nfce.modelo.Usuario;

public class UsuarioRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
    private EntityManager manager;
	
	public Usuario getById(Long id) {
		return manager.find(Usuario.class, id);
	}
	
	public void adicionar(Usuario u) throws PersistenceException {		
		manager.persist(u);
		manager.flush();
	}
	
	public Usuario login(String login, String senha){
		try { 	
			Query query = manager.createNamedQuery("Usuario.login");
			query.setParameter("login", login);
			query.setParameter("senha", senha);
			return (Usuario) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	/*
	  					Usuario usuario = (Usuario) em
                               .createQuery(
                                           "SELECT u from Usuario u where u.nomeUsuario = :name and u.senha = :senha")
                               .setParameter("name", nomeUsuario)
                               .setParameter("senha", senha).getSingleResult();
   						catch (NoResultException e) {
	 */
	
	public void editar(Usuario u) throws PersistenceException {		
		manager.merge(u);		
	}

}
