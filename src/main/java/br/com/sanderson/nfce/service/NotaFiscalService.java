package br.com.sanderson.nfce.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.NotaFiscal;
import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.modelo.repository.NotaFiscalRepository;
import br.com.sanderson.nfce.util.jpa.Transactional;

public class NotaFiscalService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	NotaFiscalRepository repository;
	
	@Transactional
	public void salvar(NotaFiscal notaFiscal) throws NegocioException {
		//Regra de negócio aqui.
		
		//Salvar
		this.repository.adicionar(notaFiscal);
	}
	
	public List<NotaFiscal> listar(){
		return repository.listar();
	}
	
	public List<NotaFiscal> findByUsuario(Usuario u){
		return repository.findByUsuario(u);
	}
	
	public List<NotaFiscal> findByUsuarioAndEstab(Usuario u, Estabelecimento estab){
		return repository.findByUsuarioAndEstabelecimento(u, estab);
	}

}
