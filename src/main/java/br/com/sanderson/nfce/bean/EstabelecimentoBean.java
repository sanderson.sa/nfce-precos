package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.service.EstabelecimentoService;

@Model
@ViewScoped
public class EstabelecimentoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<Estabelecimento> listaEstabelecimentos;
	
	@Inject
	private EstabelecimentoService service;
	
	private Estabelecimento estabelecimento = new Estabelecimento();
	
	
	
	@PostConstruct
	public void inicializar(){
		listaEstabelecimentos = service.estabelecimentos();

//		Salvar Lista de Localização
//		try {
//			for (Estabelecimento e : listaEstabelecimentos) {
//				String endereco = e.getIdEndereco().getLogradouro() + ","
//						+ e.getIdEndereco().getNumero() + ", - "
//						+ e.getIdEndereco().getBairro() + ","
//						+ e.getIdEndereco().getCidade();
//				e.setLatLng(ApacheHttpClientGet.getLatLng(endereco));
//				service.editar(e);
//			}
//		}catch(Exception e){
//			System.out.println("Erro: " + e.getMessage());
//		}
		

		//Visualizar epenas uma localização
//		String endereco = listaEstabelecimentos.get(0).getIdEndereco().getLogradouro() + ","
//				+ listaEstabelecimentos.get(0).getIdEndereco().getNumero() + ", - "
//				+ listaEstabelecimentos.get(0).getIdEndereco().getBairro() + ","
//				+ listaEstabelecimentos.get(0).getIdEndereco().getCidade();
//		System.out.println(" ## Endereço: " + endereco);
//		ApacheHttpClientGet.getLatLng(endereco);
		
	}

	public List<Estabelecimento> getEstabelecimentos() {
		return listaEstabelecimentos;
	}
	
	public void atualizarLatLng(Estabelecimento e){
		try {
			if(e.getLatlng() == null)
				service.editar(e);
		} catch (Exception e2) {
			System.out.println("Erro: " + e2.getMessage());
		}
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

}