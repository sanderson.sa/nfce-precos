//package br.com.sanderson.nfce.bean;
//
//public class Teste {
//
//	/*
//	 * /**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private Part arquivo;
//	private String cnpj;
//	private String competencia;
//	private String tipoDec;
//	//
//	private final Integer TAMANHO_REGISTRO_0001 = 10;
//	
//	private List<DeclaracaoIssCartorioReceitaMensal> listaDeclaracaoIssCartorioReceitaMensal = new ArrayList<DeclaracaoIssCartorioReceitaMensal>();
//
//    public void escolherArquivoParaImportacao() {
//        try {
//        	Scanner scanner = new Scanner(arquivo.getInputStream());
//            String conteudo = scanner.useDelimiter("\\A").next();
//            scanner.close();
//            String[] linhas = conteudo.split("\n");
//            visualizarDeclaracao(linhas);
////            for (int i = 0; i < linhas.length ; i++) {
////				System.out.println("#"+ i + "-" + linhas[i]);
////			}
////            System.out.println("## Conteúdo: " + conteudo);
//        } catch (IOException e) {
//            System.out.println("Erro: " + e.getMessage());
//        }
//    }
//    
//    private void visualizarDeclaracao(String[] registros) {
//    	for (String registro : registros) {
//    		String[] info = registro.split("\\|");
//			if(info[Constantes.DEC_TIPO_REGISTRO].equals(Constantes.REGISTRO_0000)) {
//				setCnpj(info[Constantes.REG_0000_CNPJ]);
//				setCompetencia(info[Constantes.REG_0000_COMPETENCIA_DEC]);
//				setTipoDec(info[Constantes.REG_0000_TIPO_DEC]);
//			}
//			else if(info[Constantes.DEC_TIPO_REGISTRO].equals(Constantes.REGISTRO_0001)) {
//				listaDeclaracaoIssCartorioReceitaMensal.add(setDeclaracaoIssCartorioReceitaMensal(info));
//			}
//			else if(info[Constantes.DEC_TIPO_REGISTRO].equals(Constantes.REGISTRO_0002)) {
//			}
//		}
//    }
//    
//    private DeclaracaoIssCartorioReceitaMensal setDeclaracaoIssCartorioReceitaMensal(String[] registro){
//    	DeclaracaoIssCartorioReceitaMensal decMensal = new DeclaracaoIssCartorioReceitaMensal();
//    	decMensal.setTxDeclaIssCartRecMensalConta(registro[Constantes.REG_0001_DESC_CONTA]);
//    	decMensal.setVlDeclaIssCartRecMensalUnitServico(getBigDecimal(registro[Constantes.REG_0001_VL_UNITARIO_SERVICO]));
//    	decMensal.setNuDeclaIssCartRecMensalQtdServico(Integer.parseInt(registro[Constantes.REG_0001_QUANTIDADE]));
//    	decMensal.setVlDeclaIssCartRecMensalRec(getBigDecimal(registro[Constantes.REG_0001_VL_RECEITA]));
//    	decMensal.setVlDeclaIssCartRecMensalRecTrib(getBigDecimal(registro[Constantes.REG_0001_VL_RECEITA_TRIBUTAVEL]));
//    	decMensal.setVlDeclaIssCartRecMensalAliquota(getBigDecimal(registro[Constantes.REG_0001_VL_ALIQUOTA]));
//    	decMensal.setVlDeclaIssCartRecMensalBaseCalculo(getBigDecimal(registro[Constantes.REG_0001_VL_BASE_CALCULO]));
//    	decMensal.setVlDeclaIssCartRecMensalIssDevido(getBigDecimal(registro[Constantes.REG_0001_VL_ISS]));
//    	//
//    	return decMensal;
//    }
//    
//    private BigDecimal getBigDecimal(String vl){
//    	return new BigDecimal(vl.replaceAll(",", "."));
//    }
//    
//    public void validarArquivo(){
//    	
//    }
//    
//    public void novoArquivo(){
//    	
//    }
//	
//    private String validaRegistro0001(String registro){
//    	StringBuilder validacao = new StringBuilder();
//    	String[] infos = registro.split("\\|");
//        if(infos.length != TAMANHO_REGISTRO_0001){
//        	validacao.append("Registro: " + infos[1] + " - Registro fora do layout definido no manual");
//        	return validacao.toString();
//        }
//        else if(getBigDecimal(infos[Constantes.REG_0001_VL_ISS]) == BigDecimal.ZERO){
//        	validacao.append("Registro: " + infos[1] + " - Valor de ISS Devido não pode ser 0,00");
//        }
//        return validacao.toString();
//    }
//    
//    //Validações
//    /**
//     * 
//     * @param strLinha
//     * @param nuLinha
//     * @return
//     */
//    private boolean validarImportacaoEG003(String registro, int nuLinhaSequencial){
//    	int nuLinhaRegistro;
//    	try{
//    		nuLinhaRegistro = Integer.parseInt(registro.split("|")[Constantes.REGISTRO_LINHA]);
//    		if(nuLinhaRegistro == nuLinhaSequencial) return true;
//    	}catch (NumberFormatException e) {
//			System.out.println("Erro ao executar -  validarImportacaoEG003() : " + e.getMessage());
//		}
//    	return false;
//    }
//    
////    private boolean validarImportacaoEG008(){
////    	
////    }
//    
//    // getter e setter
//	public Part getArquivo() {
//		return arquivo;
//	}
//
//	public void setArquivo(Part arquivo) {
//		this.arquivo = arquivo;
//	}
//
//	public String getCnpj() {
//		return cnpj;
//	}
//
//	public void setCnpj(String cnpj) {
//		this.cnpj = cnpj;
//	}
//
//	public String getCompetencia() {
//		return competencia;
//	}
//
//	public void setCompetencia(String competencia) {
//		this.competencia = competencia;
//	}
//
//	public String getTipoDec() {
//		if(tipoDec.equals("1")) return "Declaração Normal";
//		else if(tipoDec.equals("2")) return "Declaração Retificadora";
//		return "";
//	}
//
//	public void setTipoDec(String tipoDec) {		
//		this.tipoDec = tipoDec;
//	}
//
//	public List<DeclaracaoIssCartorioReceitaMensal> getListaDeclaracaoIssCartorioReceitaMensal() {
//		return listaDeclaracaoIssCartorioReceitaMensal;
//	}
//
//	public void setListaDeclaracaoIssCartorioReceitaMensal(
//			List<DeclaracaoIssCartorioReceitaMensal> listaDeclaracaoIssCartorioReceitaMensal) {
//		this.listaDeclaracaoIssCartorioReceitaMensal = listaDeclaracaoIssCartorioReceitaMensal;
//	}
//	 */
//	
//}
