package br.com.sanderson.nfce.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.EstabelecimentoDTO;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.modelo.repository.EstabelecimentoRepository;
import br.com.sanderson.nfce.util.jpa.JpaUtil;
import br.com.sanderson.nfce.util.jpa.Transactional;

public class EstabelecimentoService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	EstabelecimentoRepository estabelecimentoRepository;

	public List<Estabelecimento> estabelecimentos(){
		return estabelecimentoRepository.listar();
	}
	
	public List<EstabelecimentoDTO> estabelecimentos(Long idUsuario){
		return estabelecimentoRepository.getEstabelecimentosTotal(idUsuario);
	}
	
	public Estabelecimento getByCNPJ(Long cnpj) {
		return estabelecimentoRepository.getByCnpj(cnpj);
	}
	
	@Transactional
	public void salvar(Estabelecimento e) throws NegocioException {
		//Regra de negócio aqui.
		
		//Salvar
		this.estabelecimentoRepository.adicionar(e);
	}
	
	@Transactional
	public void editar(Estabelecimento e) throws NegocioException {
		//Regra de negócio aqui.
		
		//Salvar
		this.estabelecimentoRepository.editar(e);
	}
	

}
