package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.NotaFiscal;
import br.com.sanderson.nfce.modelo.Usuario;
import br.com.sanderson.nfce.service.NotaFiscalService;

@Model
@ViewScoped
public class MinhasComprasBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<NotaFiscal> notasFiscais = new ArrayList<NotaFiscal>();
		
	private Usuario usuario = new Usuario();
	private Estabelecimento estab = new Estabelecimento();
	
	@Inject
	NotaFiscalService service;

	public void inicializar() {
		notasFiscais = service.findByUsuarioAndEstab(usuario, getEstab());
	}

	public List<NotaFiscal> getNotaFiscais() {
		return notasFiscais;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getNumeroNotas(){
		return notasFiscais.size() + "";
	}
	
	public String getTotalCompras(){
		BigDecimal sum = notasFiscais.isEmpty() ? new BigDecimal(0) : notasFiscais.stream().map(NotaFiscal::getVlNfce).reduce(BigDecimal::add).get();
		return "R$" + sum.toString();
	}

	public Estabelecimento getEstab() {
		return estab;
	}

	public void setEstab(Estabelecimento estab) {
		this.estab = estab;
	}
	
}
