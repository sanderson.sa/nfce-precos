package br.com.sanderson.nfce.modelo.repository;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.NotaFiscal;
import br.com.sanderson.nfce.modelo.Usuario;

public class NotaFiscalRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public List<NotaFiscal> listar() {
		TypedQuery<NotaFiscal> query = manager.createQuery("from NotaFiscal", NotaFiscal.class);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<NotaFiscal> findByUsuario(Usuario idUsuario) {
		Query query = manager.createNamedQuery("NotaFiscal.findByUsuario");
		query.setParameter("idUsuario", idUsuario);
		List<NotaFiscal> lista = (List<NotaFiscal>) query.getResultList();
		//Collections.sort(lista, (p1, p2) -> p2.getDataEmissao().compareTo(p1.getDataEmissao()));
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotaFiscal> findByUsuarioAndEstabelecimento(Usuario idUsuario, Estabelecimento idEstab) {
		Query query = manager.createNamedQuery("NotaFiscal.findByUsuarioAndEstab");
		query.setParameter("idUsuario", idUsuario);
		query.setParameter("idEstab", idEstab);
		List<NotaFiscal> lista = (List<NotaFiscal>) query.getResultList();
		return lista;
	}
	
	public NotaFiscal getById(Long id) {
		return manager.find(NotaFiscal.class, id);
	}
	
	public void adicionar(NotaFiscal nf) throws PersistenceException {		
		manager.persist(nf);
		manager.flush();
	}
	
	public void guardar(NotaFiscal nf) throws PersistenceException {		
		manager.merge(nf);		
	}
	
	public void editar(NotaFiscal nf) {
		manager.merge(nf);
	}
	

}
