package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.service.ProdutoService;

@Model
@ViewScoped
public class ProdutoDetalheBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	
	private Produto produto = new Produto();
	
	@Inject
	ProdutoService produtoService;
	
	private List<Produto> produtos;
	
	public void inicializar(){		
		setProdutos(produtoService.getByCodigoProduto(produto.getCodigoProduto()));
		reload();
	}
	
	protected List<Integer> simpleList;
    
    public List<Integer> getSimpleList() {
        return simpleList;
    }            
 
    private void reload() {
        simpleList = new ArrayList<>();
         
        Random r = new Random();
        for (int i = 2000; i < 2010; i++) {
            simpleList.add(r.nextInt(500) + 800);    
        }  
    }
	
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public List<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

}
