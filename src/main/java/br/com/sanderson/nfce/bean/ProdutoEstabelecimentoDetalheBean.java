package br.com.sanderson.nfce.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sanderson.nfce.modelo.Estabelecimento;
import br.com.sanderson.nfce.modelo.Produto;
import br.com.sanderson.nfce.service.ProdutoService;

@Model
@ViewScoped
public class ProdutoEstabelecimentoDetalheBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	
	private Produto produto = new Produto();

	private Estabelecimento estabelecimento = new Estabelecimento();
	
	@Inject
	ProdutoService produtoService;
	
	private List<Produto> produtos;
	
	public void inicializar() {
		setProdutos(produtoService.getByEstabelecimentoAndProduto(estabelecimento.getId(),produto.getCodigoProduto()));
	}
	
	//Getters and Setters
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public List<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

}
