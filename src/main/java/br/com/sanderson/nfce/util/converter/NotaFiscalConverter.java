package br.com.sanderson.nfce.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.sanderson.nfce.modelo.NotaFiscal;
import br.com.sanderson.nfce.modelo.repository.NotaFiscalRepository;

@FacesConverter(forClass = NotaFiscal.class)
public class NotaFiscalConverter implements Converter {

	@Inject
	private NotaFiscalRepository repository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		NotaFiscal retorno = null;
		if (value != null && !"".equals(value)) {
			retorno = this.repository.getById(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			NotaFiscal object = ((NotaFiscal) value);
			return object.getId() == null ? null : object.getId().toString();
		}
		return null;
	}

}
